Cooking
=================
 
##Yii2 advanced; 
 
### Как пользоваться: ###

**Клонируем себе содержимое этого репозитория**

```sh
$ git@bitbucket.org:web-monkey/cooking.git
$ composer update
$ php init
```

**Настравиваем подключение к БД.**  

```php
/*правим файл, поменять на ваши значения серверов*/
common\config\main-local.php
//db = mysql|pgsql or your db prefix

'dsn' => '{db}:host={host};dbname={db_name}',
'username' => '{login}',
'password' => '{passwd}',
```


**Применяем миграции:**

```sh
php yii app/init
```
 
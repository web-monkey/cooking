<?php
namespace backend\assets;
use yii\web\AssetBundle;

class AdminLteAsset extends AssetBundle
{

    public static $skin = 'skin-blue';
    public static $layoutOption = 'sidebar-mini';
    public static $brandLogo = '/admin/img/logo.svg';

    public $sourcePath = '@adminlte/dist/';
    public $js = [
        'js/app.min.js',
    ];

    public $css = [
        'css/AdminLTE.min.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    /**
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect. Skin set in AppAsset
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
     */

    public function init(){
        $this->css[] = 'css/skins/'.self::$skin.'.min.css';
        parent::init();
    }
}

<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\modules\user\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use \yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * Site controller
 *
 * if need imperavi
 *
 */
class SiteController extends Controller
{
    const STAT_ERROR = 1;
    const STAT_SUCCESS = 2;
    const INIT_PAGE = '/admin/site/index';

    public $status = 1;
    public $menu;
    public $pageTitle;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['viewAp'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action){

        if (parent::beforeAction($action)) {


            if ($action->id == 'error' && $this->module->id == 'app-backend'){
                $this->layout = 'login';
            }
            /**
             * For example:
             */
            $this->menu = [

                /*'single' => [
                    'allow' => Yii::$app->user->can('changeSettings'),
                    'icon' => 'cog',
                    'label' => 'Настройки',
                    'url' => Url::toRoute('/site')
                ],
                'users' => [
                    'allow' => Yii::$app->user->can('users'),
                    'icon' => 'users',
                    'label' => Yii::t('app', 'Users'),
                    'url' => Url::toRoute('/users')
                ],*/
                'multi' => [
                    'allow' => true,
                    'label' => 'Cooking',
                    'icon' => 'birthday-cake',
                    'items' => [
                        '/cooking/ingredient' => [
                            'label' => 'Ingredient',
                            'allow' => true,
                            'url' => Url::toRoute('/cooking/ingredient')
                        ],
                        '/cooking/dish' => [
                            'label' => 'Dish',
                            'allow' => true,
                            'url' => Url::toRoute('/cooking/dish')
                        ],
                    ]
                ],
            ];

            $controller = $this->action->controller->id;
            $url = Url::toRoute('/'.Yii::$app->request->getPathInfo());

            if($controller == 'default') {
                $controller = $this->module->id;
            }

            foreach ($this->menu as $module => $item) {

                if(array_key_exists('items', $item)) {

                    foreach ($item['items'] as $elem => $childItem) {
                        if(!empty($childItem['url'])) {
                            if($childItem['url'] == $url || $controller == $module || $controller == $elem) {

                                if(!array_key_exists('htmlOptions',$this->menu[$module])) {
                                    $this->menu[$module]['htmlOptions'] = [ 'class' => ''];
                                }

                                $this->menu[$module]['htmlOptions']['class'] .= ' active';

                                if(!array_key_exists('htmlOptions', $this->menu[$module]['items'][$elem])) {

                                    $this->menu[$module]['items'][$elem]['htmlOptions'] = [
                                        'class'  => null
                                    ];

                                }

                                $this->menu[$module]['items'][$elem]['htmlOptions']['class'] .= ' active';
                                $this->pageTitle = empty($childItem['title']) ? $childItem['label'] : $childItem['title'];

                                break;
                            }
                        }
                    }

                } else if($item['url'] == $url || $controller == $module) {

                    if(!array_key_exists('htmlOptions',$this->menu[$module])) {

                        $this->menu[$module]['htmlOptions'] = [
                            'class' => null
                        ];

                    }

                    $this->menu[$module]['htmlOptions']['class'] .= ' active';
                    $this->pageTitle = empty($item['title']) ? $item['label'] : $item['title'];
                }
            }
            return true;
        } else {
            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'login';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(self::INIT_PAGE);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function echoJSON($data = array())
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        echo Json::encode($data);
    }
    public function responseStatus()
    {
        $this->echoJSON(['status' => $this->status]);
    }
    public static function cut($str, $length = 400, $adaptive = false, $enc = 'UTF-8', $ending = ' ...')
    {
        if (mb_strlen($str, $enc) > $length) {
            if ($adaptive) {
                $str = mb_substr($str, 0, $length, $enc);
                $pos = mb_strrpos($str, " ", 0, $enc);
                if ($pos > 0) {
                    return mb_substr($str, 0, $pos, $enc) . $ending;
                }
            }
            return mb_substr($str, 0, $length, $enc);
        }

        return $str;
    }

    /*
     * Дампинг запроса
     * Использование:
     * dumpQuery(Model::find()->where(['foo' => 'bar'])->orderBy(['awesome' => SORT_ASC]));
     */
    public static function dumpQuery(ActiveQuery $query)
    {
        echo '<pre>';
        var_export($query->prepare(\Yii::$app->db->queryBuilder)->createCommand(\Yii::$app->db)->rawSql);
        echo '</pre>';
        Yii::$app->end();
    }

    /**
     * @param $data
     * @param null $title
     * @throws \yii\base\ExitException
     */
    public static function exportToExcel($data, $title = null) {

        $xls = new \PHPExcel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    //'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $xls->getDefaultStyle()->applyFromArray($styleArray);

        $cells = array_slice(range('A', 'Z'), 0, count($data['cols']));

        if(!empty($title)) {
            $sheet->setTitle($title);
            // Вставляем текст в ячейку A1
            $sheet->setCellValue("A1", $title);
            $sheet->getStyle('A1')->getFill()->setFillType(
                \PHPExcel_Style_Fill::FILL_SOLID);
            $sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('00a65a');
            $sheet->mergeCells('A1:'.max($cells).'1');

            // Выравнивание текста
            $sheet->getStyle('A1')->getAlignment()->setHorizontal(
                \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }


        $LABELS_ROW_NUMBER = 2;
        $rowNum = 4;

        $i = 0;
        foreach ($data['cols'] as $col) {
            if($col['visible'] == 1) {
                $sheet->setCellValueByColumnAndRow($i, $LABELS_ROW_NUMBER, $col['label']);
                $i++;
            }
        }

        if(!empty($data['data'])) {

            foreach ($data['data'] as $item) {

                $i = 0;
                foreach ($data['cols'] as $key => $col) {


                    if($col['visible'] == 1 ) {
                        $sheet->setCellValueByColumnAndRow($i, $rowNum, (array_key_exists($key, $item) ? $item[$key] : 0));
                        $i++;
                    }
                }

                $rowNum++;
            }
        }

        foreach ($cells as $cell) {
            $sheet->getColumnDimension($cell)->setAutoSize(true);
        }

        // Выводим HTTP-заголовки
        header ( "Expires:".date('r') );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=". $title.'.xls');

        // Выводим содержимое файла
        $objWriter = new \PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');

        \Yii::$app->end();
    }
}

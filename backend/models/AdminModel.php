<?php


namespace backend\models;

use backend\controllers\SiteController;
use Yii;
use yii\db\Query;


class AdminModel extends \yii\db\ActiveRecord
{

    /**
     * @param null $id
     * @param string $key
     * @param string $val
     * @return array
     */


    const ROLE_DEV              = 1001;
    const ROLE_ADMIN            = 1;
    const ROLE_MANAGER          = 2;
    const ROLE_USER             = 3;

    public static $roleList = [
        self::ROLE_ADMIN        => 'admin',
        self::ROLE_MANAGER      => 'manager',
        self::ROLE_USER         => 'user'
    ];

    const STATUS_INVISIBLE      = 0;
    const STATUS_VISIBLE        = 1;

    public static $isVisible = [
      self::STATUS_INVISIBLE    => 'Не отображать',
      self::STATUS_VISIBLE      => 'Отображать'
    ];
    public static $yesNo = [
      self::STATUS_INACTIVE     => 'Нет',
      self::STATUS_ACTIVE       => 'Да'
    ];

    const STATUS_INACTIVE = 0;
    const STATUS_BLOCKED = 5;
    const STATUS_ACTIVE = 10;

    public static $statusLabels = [
        self::STATUS_BLOCKED    => 'Заблокированный',
        self::STATUS_INACTIVE   => 'Не активный',
        self::STATUS_ACTIVE     => 'Активный'
    ];


    public static function getValueByKey($id = null, $key = 'id', $val = 'name', $activeOnly =false)
    {
        $query = (new Query())
            ->select([$key, $val])
            ->from(static::tableName());

        if($activeOnly) {
            $query->where(['is_active' => self::STATUS_ACTIVE]);
        }


        if(empty($id)) {
            $response = [];
            $data = $query->all();

            if(!empty($data)) {
                foreach($data as $elem) {
                    $response[$elem[$key]] = $elem[$val];
                }
            }

        } else {
            $elem = $query
                ->andWhere([
                    'id' => intval($id)
                ])
                ->one();

            $response[$elem[$key]] = $elem[$val];
        }


        return $response;
    }

    /**
     * @param null $id
     * @param string $prop
     * @return bool|string
     */
    public static function getPropByKey($id = null, $prop = 'name')
    {
        return (new Query())
            ->select($prop)
            ->from(static::tableName())
            ->andWhere([
                'id' => intval($id)
            ])
        ->scalar();
    }


    public function prepareDate($attribute, $params) {
        $this->$attribute = strtotime($this->$attribute);
    }

    /**
     * @param $class
     * @deprecated
     * @return array
     */

    public  static function findGridData($class)
    {

        $structure = $class::renderStructure();
        $selectedCol = [];

        if(!empty($structure)) {

            foreach ($structure as $col => $props) {

                if(isset($props['show']) && $props['show']) {
                    $selectedCol[$col] = $props;
                }
            }
        }

        $selectedCol = empty($selectedCol) ? '*' : $selectedCol;

        $order = Yii::$app->request->getQueryParam('order','DESC');
        $orderBy = Yii::$app->request->getQueryParam('by','id');
        $limit = Yii::$app->request->getQueryParam('limit',50);
        $page = Yii::$app->request->getQueryParam('page',1);
        $dropDownFilter = Yii::$app->request->getQueryParam('dropDownFilter');
        $dateRangeFilter = Yii::$app->request->getQueryParam('dateRangeFilter');
        $queryString = Yii::$app->request->getQueryParam('query');
        $searchBy = Yii::$app->request->getQueryParam('searchBy');


        $query = (new Query())
            ->select(array_keys($selectedCol))
            ->from($class::tableName());
        
        if(!empty($queryString)) {
            $query->andWhere([
                'LIKE',$searchBy, $queryString
            ]);
        }

        if(!empty($dropDownFilter)) {

            foreach (json_decode($dropDownFilter,true) as $filter => $value) {
                if(!empty($value)) {
                    $query->andWhere([
                        $filter => $value
                    ]);
                } else if($filter == 'status' && $value !== null) {
                    $query->andWhere([
                        $filter => 0
                    ]);
                }

            }
        }

        if(!empty($dateRangeFilter)) {
            $dates = self::prepareDateRange($dateRangeFilter);
            $query
                ->andWhere(['between','created_at',$dates['start'], $dates['end']]);
        }



        $rowCount = $query->count();

        settype($page,'int');
        $offset = ($page * $limit) - $limit;

        if($offset > 0) {
            $query->offset($offset);
        }

        $data = $query
            ->limit($limit)
            ->orderBy($orderBy.' '.$order)
            ->all();

        $giftList = [];

        if(!empty($data)) {
            foreach ($data as $fb) {

                $props = [];
                foreach ($fb as $key => $value) {
                    $relation = null;
                    $props[$key] = SiteController::prepareNgGridText($selectedCol[$key], $value);
                }

                $giftList[] = $props;
            }
        }


        $pageCount = ceil($rowCount / $limit);

        return [
            'cols' => $selectedCol,
            'data' => $giftList,
            'pageCount' => $pageCount,
        ];
    }


    /**
     * @param $range mixed string|null
     * @return array
     */
    public static function prepareDateRange($range)
    {
        if(empty($range)) {
            $start = strtotime(date('d.m.Y 00:00'));
            $end = strtotime(date('d.m.Y 23:59:59'));
        } else {

            $dates = explode('-', $range);
            $start = empty($dates[0]) ? strtotime(date('d.m.Y 00:00')) : strtotime($dates[0].' 00:00');
            $end = empty($dates[1]) ? strtotime(date('d.m.Y 23:59:59',$start)) : strtotime($dates[1].' 23:59:59');
        }
        return ['start' => $start, 'end' => $end];
    }

    public static function onlyStrTableName($tableName)
    {
        return str_replace(['{{%','}}'], '', $tableName);
    }
}
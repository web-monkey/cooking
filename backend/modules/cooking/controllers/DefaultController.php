<?php

namespace backend\modules\cooking\controllers;

use backend\controllers\SiteController;

/**
 * Default controller for the `cooking` module
 */
class DefaultController extends SiteController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

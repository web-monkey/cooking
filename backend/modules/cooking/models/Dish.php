<?php

namespace backend\modules\cooking\models;

use backend\models\AdminModel;
use Yii;
use \backend\modules\cooking\models\query\DishQuery;

/**
 * This is the model class for table "{{%dish}}".
 *
 * @property integer $id
 * @property integer $is_visible
 * @property string $name
 *
 * @property Recipe[] $recipe
 */
class Dish extends AdminModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dish}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_visible'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'is_visible' => Yii::t('backend', 'Is Visible'),
            'name' => Yii::t('backend', 'Name'),
            'recipe' => Yii::t('backend', 'Recipe'),
        ];
    }

    public function getRecipe()
    {
        return $this->hasMany(Recipe::class,['dish_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return DishQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DishQuery(get_called_class());
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $relatedItems = Yii::$app->request->post('related');

        if(empty($relatedItems)) {
            Recipe::deleteAll(
                ['dish_id' => $this->id]
            );
        } else {
            //insert or update and other delete
            $relatedItems = json_decode($relatedItems);
            $currentItems = [];

            foreach ($relatedItems as $relatedItem) {

                if( ($item = Recipe::find()
                        ->where([
                            'dish_id' => $this->id,
                            'ingredient_id' => $relatedItem->id
                        ])->one()) === null) {
                    $item = new Recipe();
                    $item->dish_id = $this->id;
                    $item->ingredient_id = $relatedItem->id;
                }

                $item->count = $relatedItem->count;
                if($item->save()) {
                    $currentItems[] = $item->getPrimaryKey();
                }
            }

            Recipe::deleteAll(
                [
                    'AND',
                    ['dish_id' => $this->id],
                    ['NOT IN', 'id' , $currentItems]
                ]
            );

        }

    }
}

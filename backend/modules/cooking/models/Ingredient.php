<?php

namespace backend\modules\cooking\models;

use backend\models\AdminModel;
use Yii;
use \backend\modules\cooking\models\query\IngredientQuery;
/**
 * This is the model class for table "{{%ingredient}}".
 *
 * @property integer $id
 * @property integer $is_visible
 * @property string $name
 */
class Ingredient extends AdminModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ingredient}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_visible'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'is_visible' => Yii::t('backend', 'Is Visible'),
            'name' => Yii::t('backend', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return IngredientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IngredientQuery(get_called_class());
    }
}

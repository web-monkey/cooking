<?php

namespace backend\modules\cooking\models;

use backend\models\AdminModel;
use Yii;
use \backend\modules\cooking\models\query\RecipeQuery;

/**
 * This is the model class for table "{{%recipe}}".
 *
 * @property integer $id
 * @property integer $dish_id
 * @property integer $ingredient_id
 * @property double $count
 *
 * @property Dish $dish
 * @property Ingredient $ingredient
 */
class Recipe extends AdminModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%recipe}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dish_id', 'ingredient_id'], 'integer'],
            [['count'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'dish_id' => Yii::t('backend', 'Dish ID'),
            'ingredient_id' => Yii::t('backend', 'Ingredient ID'),
            'count' => Yii::t('backend', 'Count'),
        ];
    }

    public function getDish()
    {
        return $this->hasOne(Dish::class,['id' => 'dish_id']);
    }
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::class,['id' => 'ingredient_id']);
    }

    /**
     * @inheritdoc
     * @return RecipeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RecipeQuery(get_called_class());
    }
}

<?php

namespace backend\modules\cooking\models\query;

use \backend\modules\cooking\models\Ingredient;

/**
 * This is the ActiveQuery class for [[\backend\modules\cooking\models\Ingredient]].
 *
 * @see \backend\modules\cooking\models\Ingredient
 */
class IngredientQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere([
            'is_visible' => Ingredient::STATUS_VISIBLE
        ]);
    }

    /**
     * @inheritdoc
     * @return Ingredient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ingredient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

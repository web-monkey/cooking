<?php

namespace backend\modules\cooking\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\cooking\models\Recipe]].
 *
 * @see \backend\modules\cooking\models\Recipe
 */
class RecipeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cooking\models\Recipe[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cooking\models\Recipe|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

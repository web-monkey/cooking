<?php

namespace backend\modules\cooking\models\search;

use backend\modules\cooking\models\Ingredient;
use backend\modules\cooking\models\Recipe;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cooking\models\Dish;
use yii\db\Query;

/**
 * DishSearch represents the model behind the search form about `backend\modules\cooking\models\Dish`.
 */
class DishSearch extends Dish
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_visible'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dish::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andWhere(['NOT IN', 'id', (new  Query())
            ->select('dish_id')
            ->from(Recipe::tableName().' r')
            ->leftJoin(Ingredient::tableName().' i', 'ingredient_id = i.id')
            ->andWhere(['is_visible' => static::STATUS_INVISIBLE])
            ->column()]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_visible' => $this->is_visible,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}

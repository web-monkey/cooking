<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use \backend\modules\cooking\models\Ingredient;
$related = [];
/* @var $this yii\web\View */
/* @var $model backend\modules\cooking\models\Dish */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dish-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <!--related items-->
    <div class="tab-content item-related">
        <?=Html::activeLabel($model,'recipe')?>
        <table class="table table-bordered table-condensed vertical-middle" style="max-width: 600px;">
            <thead>
            <tr>
                <th>Ingredient</th>
                <th style="width: 120px;">Count</th>
                <th style="width: 84px;"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($model->recipe)) {
                foreach ($model->recipe as $element) {
                    $related[] = [
                            'id' => $element->ingredient_id,
                            'count' => $element->count,
                    ]
            ?>
                    <tr data-id="<?= $element->ingredient_id; ?>" data-count="<?= $element->count; ?>">
                        <td><?= $element->ingredient->name; ?></td>
                        <td class="text-center"><?= $element->count ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-sm btn-danger btn-remove">
                                <i class="fa fa-remove"></i>
                            </button>
                        </td>
                    </tr>
                <?php }
            }
            ?>
            </tbody>
            <tfoot>
            <tr>
                <td>
                    <?= Html::dropDownList('item', null, ArrayHelper::map(Ingredient::find()->active()->all(), 'id', 'name'), ['class' => 'form-control']) ?>
                </td>
                <td>
                    <input type="text" class="form-control">
                </td>
                <td class="text-center">
                    <button type="button" class="btn btn-sm btn-primary btn-add"><i class="fa fa-plus"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>
        <?=Html::hiddenInput('related', json_encode($related))?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

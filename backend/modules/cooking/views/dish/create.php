<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cooking\models\Dish */

$this->title = Yii::t('backend', 'Create Dish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Dishes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dish-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

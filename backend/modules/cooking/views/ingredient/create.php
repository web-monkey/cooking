<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cooking\models\Ingredient */

$this->title = Yii::t('backend', 'Create Ingredient');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ingredients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cooking\models\Ingredient */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Ingredient',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ingredients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="ingredient-update">

    <p>
        <?= Html::a(Yii::t('backend', 'Create Ingredient'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

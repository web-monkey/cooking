<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\controllers\SiteController;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\log\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="log-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'level',
                'filter' => Html::activeDropDownList($searchModel, 'level',SiteController::$errorLevels,['class'=>'form-control','prompt' => 'Все']),
                'value' => function($model) {
                    return isset(SiteController::$errorLevels[$model->level]) ? SiteController::$errorLevels[$model->level] : 'unknown';
                }
            ],

            'category',
            //'log_time',
            //'prefix:ntext',
             'message:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} '
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php

namespace backend\modules\user;

/**
 * user module definition class
 *
 *
'multi' => [
'allow' => true,
'label' => 'multi',
'icon' => 'sort-desc',
'items' => [
'check/default/index' => [
'label' => '1',
'allow' => true,
'url' => '/admin/site/test1'
],
'raffle/default/index' => [
'label' => '2',
'allow' => true,
'url' => '/admin/site/test2'
],
]
],
 */
class User extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\user\controllers';

   /* public static $menu = [
        ''
    ];*/

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

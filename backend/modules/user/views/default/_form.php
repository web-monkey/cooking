<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \backend\modules\user\models\User;
/* @var $this yii\web\View */
/* @var $model User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
    <?php // $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'autocomplete' =>'off']) ?>
    <?= $form->field($model, 'status')->dropDownList(User::$statusLabels); ?>


    <?= $form->field($model, 'role')->radioList(User::$roleList); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

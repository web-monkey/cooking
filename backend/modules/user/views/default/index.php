<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            /*'lastname',
            'firstname',*/
            'email:email',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',

            [
                'attribute' => 'status',
                'value' => function($model) {
                    return User::$statusLabels[$model->status];
                },
                'filter' => Html::activeDropDownList($searchModel, 'status',User::$statusLabels,['class'=>'form-control','prompt' => 'Все']),
            ],
[
                'attribute' => 'role',
                'value' => function($model) {
                    return User::$roleList[$model->role];
                },
                'filter' => Html::activeDropDownList($searchModel, 'status',User::$roleList,['class'=>'form-control','prompt' => 'Все']),
            ],

             'created_at:date',
             'updated_at:date',
             'blocked_at:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

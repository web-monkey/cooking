<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\modules\user\models\User;
/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('app','Create'), ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email:email',
            [
                'attribute' => 'status',
                'value' => User::$statusLabels[$model->status]
            ],
            [
                'attribute' => 'role',
                'value' => User::$roleList[$model->role]
            ],
            'created_at:date',
            'updated_at:date',
            'blocked_at:date',
        ],
    ]) ?>

</div>

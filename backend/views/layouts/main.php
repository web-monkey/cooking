<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\assets\AdminLteAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
if(empty($this->title)) {
    $this->title = $this->context->pageTitle;
}
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition <?=AdminLteAsset::$skin.' '.AdminLteAsset::$layoutOption?>">
    <script type="text/javascript">var menuState = localStorage.getItem('left_menu_state');
        if (typeof menuState !== 'undefined' && menuState == 'collapse') {
            document.getElementsByTagName('body')[0].className += " sidebar-collapse"
        }
    </script>
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">
            <a href="<?=Yii::$app->homeUrl;?>" class="logo">
                <span class="logo-mini"> <?=Html::img(AdminLteAsset::$brandLogo,['alt' => Yii::$app->name]);?></span>
                <span class="logo-lg"> <?=Html::img(AdminLteAsset::$brandLogo,['alt' => Yii::$app->name]);?> <?=Yii::$app->name?></span>
            </a>
            <?= $this->render('parts/menu_top'); ?>
        </header>
        <?= $this->render('parts/menu_left'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="content-header">
                <h1><?= Html::encode($this->title) ?></h1>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>
            <section class="content">
                <?= Alert::widget() ?>
                <?= $content ?>
            </section>
        </div>

        <footer class="main-footer">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                Anything you want
            </div>
            <!-- Default to the left -->
             <strong> &copy; WMStarterKit <?=date('Y')?>  <!--<a href="#">WMSK</a>.</strong> All rights reserved.-->
        </footer>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
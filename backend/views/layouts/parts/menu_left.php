<?php

use yii\helpers\Url;
use \common\helpers\Html;
/**
 * @var $this \yii\web\View
 * @var \backend\controllers\SiteController $ctrl
 */

$ctrl = $this->context;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!--<li class="header">HEADER</li>-->
            <?php if(!empty($ctrl->menu)) {
                foreach( $ctrl->menu as  $url =>  $el) {
                    if(array_key_exists('allow', $el) && $el['allow']) {

                        if(empty($el['items'])) { ?>

                            <li <?php
                            if(!empty($el['htmlOptions'])) {
                                foreach($el['htmlOptions'] as $param => $val) {
                                    echo $param.'="'.$val.'" ';
                                }
                            }
                            ?>
                            >
                                <?php
                                echo  Html::a(
                                    empty($el['icon']) ? Html::tag('span',$el['label']) : Html::icon($el['icon']).Html::tag('span',$el['label']),
                                    Url::to(empty($el['url']) ? '#' : $el['url'])
                                ); ?>
                            </li>

                        <?php } else { ?>

                            <li
                                <?php
                                    if(!empty($el['htmlOptions'])) {
                                        foreach($el['htmlOptions'] as $param => $val) {
                                            echo $param.'="'.$val.'" ';
                                        }
                                    }
                                ?>
                            >
                                <a href="#">
                                    <?php if(array_key_exists('icon', $el)) { echo Html::icon($el['icon']); }?>
                                    <span><?=$el['label']?></span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php foreach($el['items'] as  $childUrl =>  $child) {?>
                                        <li
                                            
                                            <?php
                                            
                                            if(!empty($child['htmlOptions'])) {
                                                foreach($child['htmlOptions'] as $param => $val) {
                                                    echo $param.'="'.$val.'" ';
                                                }
                                            }
                                            ?>

                                        >
                                            <?php
                                            echo  Html::a(
                                                empty($child['icon']) ? Html::tag('span',$child['label']) : Html::icon($child['icon']).Html::tag('span',$child['label']),
                                                Url::to(empty($child['url']) ? '#' : $child['url'])
                                            ); ?>
                                        </li>
                                    <?php }?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                }
            } ?>
        </ul>
    </section>
</aside>
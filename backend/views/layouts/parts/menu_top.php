<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="/admin/site/logout" data-method="post"> <!-- class="dropdown-toggle" data-toggle="dropdown"-->
                    <i class="fa fa-user"></i> <span class="hidden-xs"><?=Yii::$app->user->identity->email;?></span>
                </a>
            </li>
        </ul>
    </div>
</nav>
<!--сделать еще crud для юзеров с учетом ролей;-->
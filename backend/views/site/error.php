<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <?=Html::tag('p',Yii::t('common','Please contact us if you think this is a server error. Thank you.'))?>
    <p>
        <?=Html::a(Yii::t('common','Home page'),\yii\helpers\Url::home())?>
    </p>

</div>

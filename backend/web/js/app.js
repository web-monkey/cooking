'use strict';

$(document).ready(function () {

    var $body = $('body');

    $body.on('change','.upload',function () {
        var reader = new FileReader();
        var imgUploadWrap = $(this).closest('.upload-preview');

        reader.onload = function (e) {
            imgUploadWrap.find('img').prop('src', e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
    }).on('click','.sidebar-toggle',function(){
        setTimeout(function () {
            localStorage.setItem('left_menu_state',$('body').hasClass('sidebar-collapse') ? 'collapse' : null);
        }, 900);
    });

    var $itemRelated = $('.item-related');

    if($itemRelated.length) {

        $itemRelated.on('click','.btn-add', function() {
            var $target = $(this),
                $tr = $target.closest('tr');

            var count = $tr.find('input[type=text]').val();
            var selectedItem = $tr.find('select option:selected');


            $target.closest('table').find('tbody').append('<tr data-id="' + selectedItem.val() + '" data-count="' + count + '">\
            <td>' + selectedItem.text() + '</td>\
            <td class="text-center">' + count + '</td>\
            <td class="text-center">\
                <button type="button" class="btn btn-sm btn-danger btn-remove"><i class="fa fa-remove"></i></button>\
                </td>\
                </tr>');

            prepareRelated();
        }).on('click','.btn-remove', function() {
            $(this).closest('tr').remove();
            prepareRelated();
        });

    }

    function prepareRelated() {
        var items = [];
        $.each($itemRelated.find('tbody tr'), function(i, el){
            var $el = $(el);

            items.push({
                id:$el.data('id'),
                count: $el.data('count')
            });
        });

        $itemRelated.find('input[name=related]').val(JSON.stringify(items));

    }

});
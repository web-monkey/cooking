<?php

namespace common\components;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "{{%file_list}}".
 *
 * @property integer $id
 * @property integer $is_visible
 * @property string $path
 * @property string $alt
 * @property string $title
 * @property string $_sort
 */
class FileList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_visible', '_sort'], 'integer'],
            [['path', 'alt', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_visible' => 'Is Visible',
            'path' => 'Path',
            'alt' => 'Alt',
            'title' => 'Title',
            '_sort' => 'Sort',
        ];
    }

    public static function getGallery($ids)
    {
        if(empty($ids)) {
            return null;
        } else {

            if(!is_array($ids)) {
                $ids = explode(',',$ids);
            }

            return (new Query())
                ->from(self::tableName())
                ->where(
                    ['IN','id', $ids]
                )
                ->orderBy('_sort ASC')
            ->all();
        }
    }
}

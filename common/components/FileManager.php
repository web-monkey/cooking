<?php

namespace common\components;

use \Yii;
use \yii\base\Component;
use yii\db\Query;
use yii\web\UploadedFile;

/**
 * Yii FileManger v0.1
 * class for managing files (uploading (form/ajax), downloading, allocation and resizing)
 *
 * require classes:
 * - FMFile (file managing)
 * - FMResizer (image resizing)
 * - FileController (enter point for uploading and downloading files + repository creation)
 *
 * uses to DB tables:
 * - repository table -> saving repositories and its settings
 * - files table -> saving uploaded files info (all files are linked to repository)
 *
 * "Plupload" can be used natively for ajax file uploading
 *
 * @example: file adding to repository
 *  FileManager::getInstance()
 *      ->setRepository('example_repo')
 *      ->addFile($uploadedFileInstance);
 *
 * TODO make it fully Yii component (extension)? (using like Yii::app()->fm->...)
 * TODO add creator/owner relation to files
 *
 * @author maximum
 */

class FileManager extends Component {

    /**
     * @var FileManager singleton instance
     */
    public static $_instance;
    /**
     * @var bool whether plupload js file was already registered on page
     */
    private static $_pluploadRegistered = false;

    private static $_assetsPath = '';

    /*
     * path alias to files folder from webroot
     */
    const FILES_ROOT_DIRECTORY = 'files';
    /*
     * file manager table names
     */
    const REPOSITORY_TABLE = 'file_manager';
    const FILES_TABLE = 'file_list';

    const PLUPLOAD_JS_FILE = 'pluploadInitializer.js';
    const REPOSITORY_TYPE_IMAGE = 1;
    const REPOSITORY_TYPE_VIDEO = 2;

    const DEF_NESTING = 2;
    const HASH_SPLIT_LEN = 2;

    /**
     * @var integer active repository id
     */
    private $_activeId = false;
    /**
     * @var string repository name
     */
    private $_activeRepo = false;
    /**
     * @var integer
     * custom identifier in repository group
     * only integer for now (probably id of corresponding object used here)
     */
    private $_activeSection = false;
    /**
     * @var array repository settings
     */
    private $_settings = array();
    /**
     * @var string root files directory (applies to FILES_ROOT_DIRECTORY)
     */
    private $_rootPath = '';

    private $_tmpPath = '';

    /**
     * @var bool list of errors occurred
     */
    public $_errors = false;

    // errors (working with repository)
    const ER_REPOSITORY_ALREADY_EXISTS = 1;
    const ER_REPOSITORY_CREATION = 2;
    const ER_REPOSITORY_NOT_EXIST = 3;
    const ER_REPOSITORY_NOT_SPECIFIED = 4;
    const ER_REPOSITORY_DIR_CREATION_FAILED = 5;
    const ER_REPOSITORY_SETTINGS_ERROR = 6;
    const ER_WRONG_SECTION_ID = 7;
    const ER_SECTION_ID_NOT_SPECIFIED = 8;

    // errors (working with files)
    const ER_PATH_GENERATION_FAILED = 11;
    const ER_PATH_CREATION_FAILED = 12;
    const ER_FILE_COPYING = 13;
    const ER_FILE_WRONG_INPUT = 14;
    const ER_FILE_MOVING = 15;
    const ER_FILE_MOVING_UPLOADED = 16;

    // file check errors
    const ER_CHECK_MIME = 21;
    const ER_CHECK_SIZE_MIN = 22;
    const ER_CHECK_SIZE_MAX = 23;
    const ER_CHECK_EXT = 24;

    // resizer errors
    const ER_RESIZER_FNF = 31;
    const ER_RESIZER_INVALID_PARAMS = 32;
    const ER_RESIZER_FILE_COPYING = 33;
    const ER_RESIZER_FILE_SAVING = 34;
    const ER_RESIZER_NOT_IMAGE = 35;
    const ER_RESIZER_FILE_REWRITE = 36;

    /**
     * @return FileManager self creator
     */
    public static function getInstance() {
        return (isset(self::$_instance))
            ? self::$_instance
            : self::$_instance = new FileManager();
    }

    function __construct() {
        /*Yii::app()->setImport(array(
            'application.components.FMResizer',
        ));*/

        /*$this->_rootPath = Yii::getPathOfAlias('webroot')
            . DIRECTORY_SEPARATOR . self::FILES_ROOT_DIRECTORY;
        $this->_tmpPath = $this->_rootPath . DIRECTORY_SEPARATOR . 'tmp';*/
        $this->_rootPath = Yii::getAlias('@app')
            . '/' . self::FILES_ROOT_DIRECTORY;
        $this->_tmpPath = $this->_rootPath . '/tmp';

    }

    /*------------ GETTERS ---------------*/

    public function getId() {
        return $this->_activeId;
    }

    public function getSection() {
        return $this->_activeSection;
    }

    public function getRootPath($abs = true) {
        if ($abs === false) {
            //return DIRECTORY_SEPARATOR . self::FILES_ROOT_DIRECTORY;
            return '/' . self::FILES_ROOT_DIRECTORY;
        }

        return $this->_rootPath;
    }

    public function getNesting() {
        return (is_numeric($this->_settings['nesting']) && $this->_settings['nesting'] > 0)
            ? $this->_settings['nesting']
            : self::DEF_NESTING;
    }

    public function getRepositoryName() {
        if (!$this->_activeRepo) {
            $this->setError(self::ER_REPOSITORY_NOT_SPECIFIED);
            return false;
        }

        return $this->_activeRepo;
    }

    public function getRepositoryType() {
        return (isset($this->_settings['type'])) ? $this->_settings['type'] : 0;
    }

    public function getSizeLimits() {
        $limits = array(
            isset($this->_settings['min_size']) ? $this->_settings['min_size'] : 0,
            isset($this->_settings['max_size']) ? $this->_settings['max_size'] : 0,
        );

        return ($limits[0] === 0 && $limits[1] === 0) ? false : $limits;
    }

    public function getAllowedExt() {
        return (empty($this->_settings['allowed_ext']))
            ? false
            : ((is_array($this->_settings['allowed_ext']))
                ? $this->_settings['allowed_ext']
                : explode(',', $this->_settings['allowed_ext']));
    }

    public function getResizeList() {
        return (empty($this->_settings['resize']))
            ? false
            : ((is_array($this->_settings['resize']))
                ? $this->_settings['resize']
                : unserialize($this->_settings['resize']));
    }

    /**
     * register plupload and file manager scripts and add them to page
     */
    public static function registerScripts() {
        // TODO make runtimes selectable
        if (!self::$_pluploadRegistered) {
            self::$_assetsPath = Yii::$app->getAssetManager()
                ->publish(Yii::getAlias('@common/components/plupload'));


            Yii::$app->view->registerJsFile(self::$_assetsPath . '/plupload.full.js', ['depends' => '\backend\assets\AppAsset']);
            Yii::$app->view->registerJsFile(self::$_assetsPath. '/' . self::PLUPLOAD_JS_FILE, ['depends' => '\backend\assets\AppAsset']);

            /*
            $cs->registerScriptFile(self::$_assetsPath . '/plupload.full.js', CClientScript::POS_HEAD);
            $cs->registerScriptFile(self::$_assetsPath. '/' . self::PLUPLOAD_JS_FILE, CClientScript::POS_HEAD);*/


            self::$_pluploadRegistered = true;
        }
    }

    public static function getAssetsPath() {
        self::registerScripts();

        return self::$_assetsPath;
    }

    /* --------------- MANAGER INITIALIZER ------------------ */

    public function initializeManager() {
        $tableList = Yii::$app->db->createCommand('SHOW TABLES')
            ->queryColumn();

        // create repository table (if not exists)
        if (!in_array(self::REPOSITORY_TABLE, $tableList)) {
            Yii::$app->db->createCommand()
                ->createTable(
                    self::REPOSITORY_TABLE,
                    $this->repositoryTable()
                );
        }

        // create files table (if not exists)
        if (!in_array(self::FILES_TABLE, $tableList)) {
            Yii::$app->db->createCommand()
                ->createTable(
                    self::FILES_TABLE,
                    $this->filesTable()
                );
        }

        // TODO create indexes
        // create root files directory
        if (!is_dir($this->_tmpPath)) {
            mkdir($this->_tmpPath, 0777, true);
        }
    }

    public function createRepository($name, $arParams) {
        $repository = (new Query())
            ->select('id')
            ->from(self::REPOSITORY_TABLE)
            ->where('name=:name', array(':name' => $name))
            ->scalar();

        if ($repository) {
            $this->setError(self::ER_REPOSITORY_ALREADY_EXISTS);
            return false;
        }

        // set name
        $arParams['name'] = $name;
        // resolve repository type
        if (isset($arParams['type'])) {
            switch ($arParams['type']) {
                case self::REPOSITORY_TYPE_IMAGE:
                case 'image':
                    $arParams['type'] = self::REPOSITORY_TYPE_IMAGE;
                    break;
                case self::REPOSITORY_TYPE_VIDEO:
                case 'video':
                    $arParams['type'] = self::REPOSITORY_TYPE_VIDEO;
                    break;
                default: $arParams['type'] = 0;
            }
        }
        // other settings
        $arParams['secured'] = (isset($arParams['secured']) && $arParams['secured'] === true) ? 1 : 0;
        $arParams['min_size'] = (isset($arParams['min_size'])) ? $this->getNumericSize($arParams['min_size']) : 0;
        $arParams['max_size'] = (isset($arParams['max_size'])) ? $this->getNumericSize($arParams['max_size']) : 0;

        // wrap resize settings
        $arParams['resize'] = serialize((isset($arParams['resize'])) ? $arParams['resize'] : array());

        // create folder for repository files
        $this->createRepositoryFolder($name);

        return Yii::$app->db->createCommand()
            ->insert(self::REPOSITORY_TABLE, $arParams);
    }

    public function createRepositoryList($list) {
        $result = false;
        if (is_array($list) && !empty($list)) {
            foreach($list as $name => $set) {
                $result &= $this->createRepository($name, $set);
            }
        }

        return $result;
    }

    private function createRepositoryFolder($name) {
        $newDirPath = $this->getRootPath() . DIRECTORY_SEPARATOR . $name;
        if (!is_dir($newDirPath)) {
            // make it recursive just in case
            if (!mkdir($newDirPath, 0777, true)) {
                $this->setError(self::ER_REPOSITORY_DIR_CREATION_FAILED);
                return false;
            }
            // TODO create .htaccess rules if repository is secured
        }
        return true;
    }

    /**
     * @return array DB table structure for repository table
     */
    private function repositoryTable() {
        return array(
            'id'        => 'pk',
            'name'      => 'varchar(32) NOT NULL',
            'type'      => 'tinyint(1) DEFAULT 0',
            'secured'   => 'tinyint(1) NOT NULL DEFAULT 0',
            'allowed_ext' => 'text',
            'nesting'   => 'tinyint(2)',
            'min_size'  => 'int DEFAULT 0',
            'max_size'  => 'int DEFAULT 0',
            'resize'  => 'text',
        );
    }

    /**
     * @return array DB table structure for files table
     */
    private function filesTable() {
        return array(
            'id'    => 'pk',
            'repository_id' => 'int(11) NOT NULL',
            //'repository_name' => 'varchar(32) NOT NULL',
            'section' => 'int(11)', // field for custom grouping attribute (some id probably)
            'path'  => 'varchar(64) NOT NULL',
            'ext'   => 'varchar(4)',
            'mime'  => 'varchar(32)',
            'original_name' => 'varchar(256)',
            'size'  => 'int(11) NOT NULL',
            'date'  => 'int(10)',
            'used'  => 'tinyint(1) DEFAULT 0',
            //'secured'   => 'tinyint(1) DEFAULT 0',    // commented for now
            'title' => 'varchar(256)',
            'alt'   => 'varchar(256)',
            'url'   => 'varchar(256)',
            'position'=> 'int(11)',
        );
    }

    /**
     * @param $repository - repository name
     * @return FileManager
     * set active repository
     */
    public function setRepository($repository){
        // check whether repository exists
        // and get its settings

        $settings = (new Query())
            ->select('id,type,secured,allowed_ext,nesting,min_size,max_size,resize')
            ->from(self::REPOSITORY_TABLE)
            ->where('name=:name', array(':name' => $repository))
            ->one();

        if (!$settings) {
            $this->setError(self::ER_REPOSITORY_NOT_EXIST);
            return $this;
        }

        // create repository folder (if needed)
        $this->createRepositoryFolder($repository);

        // set active repository
        $this->_activeRepo = $repository;
        $this->_activeId = $settings['id'];
        unset($settings['id']);

        // section drop automatically when repository is set or chosen
        $this->unsetSection();

        $settings['resize'] = unserialize($settings['resize']);
        $this->_settings = $settings;

        return $this;
    }

    /**
     * @param $repository
     * @return int
     * finds repo id by name
     */
    public static function getRepositoryId($repository) {
        return (new Query())
            ->select('id')
            ->from(self::REPOSITORY_TABLE)
            ->where('name=:name', array(':name' => $repository))
            ->scalar();
    }

  /*
  РЅР°Р·РІР°С‚СЊ РєР°Рє РЅРёР±СѓС‚СЊ С„СѓРЅРєС†РёСЋ СЃ СЃРґРµР»Р°С‚СЊ С‚Р°Рє С‡С‚Рѕ Р±С‹ РІРѕР·РІСЂР°С‰Р°Р»РѕСЃСЊ РЅР°Р·РІР°РЅРёРµ СЂРµРїРѕ РїРѕ РµРіРѕ РёРґ
   public static function getRepositoryName($repository) {
        return Yii::$app->db->createCommand()
            ->select('id')
            ->from(self::REPOSITORY_TABLE)
            ->where('name=:name', array(':name' => $repository))
            ->queryScalar();
    }*/

    /**
     * @param $sectionId
     * @return FileManager
     * set active section for chosen repository
     */
    public function setSection($sectionId) {
        if (is_numeric($sectionId)) {
            $this->_activeSection = $sectionId;
        } else {
            $this->setError(self::ER_WRONG_SECTION_ID);
        }

        return $this;
    }

    /**
     * unset section setting for chosen repository
     */
    public function unsetSection() {
        $this->_activeSection = false;
    }

    /* ------------- WORKING WITH FILES -------------- */

    /**
     * @param UploadedFile|string $uploadedFile
     * @param bool          $used
     * @param bool          $delSource
     * @return bool
     * check uploaded file and move from tmp folder
     * create resizes for images (if needed)
     * make table record for file
     */
    public function addFile($uploadedFile, $used = true, $delSource = true){

        $file = new FMFile();



        // if uploaded using Yii form
        if ($uploadedFile instanceof UploadedFile) {
            $usedRuntime = 'yii';
        } elseif (!empty($uploadedFile)) {
            $usedRuntime = 'plupload';
        } else {
            $this->setError(self::ER_FILE_WRONG_INPUT);
            return false;
        }

        $file->fillData($usedRuntime, $uploadedFile);
        $file->resizeKeys = $this->getResizeKeyList();
        $file->used = $used;



        // proceed file check due to repository settings
        if (!$this->checkFile($file)) {
            return false;
        }

        // register new file in files table
        $fileID = $file->addFileToRepo($this->getId(), $this->getSection());


        // generate file path
        if (!($file->path = $this->generateFilePath($file))) {
            $file->delIncompleteRecord();
            return false;
        }

        // leave only path here
        if (is_array($uploadedFile)) {
            $uploadedFile = $uploadedFile['path'];
        }

        // moving file to repository
        if ($usedRuntime == 'yii') {
            if ($uploadedFile->saveAs($this->getRootPath() . $file->path, $delSource)) {
                $this->setError(self::ER_FILE_MOVING);
                $file->delIncompleteRecord();
                return false;
            }
        } else {
            if (is_uploaded_file($uploadedFile)) {
                if (!move_uploaded_file($uploadedFile, $this->getRootPath() . $file->path)) {
                    $file->delIncompleteRecord($fileID);
                    $this->setError(self::ER_FILE_MOVING_UPLOADED);
                    return false;
                }
            } else {
                if ($delSource) {
                    if (!rename($uploadedFile, $this->getRootPath() . $file->path)) {
                        $file->delIncompleteRecord($fileID);
                        $this->setError(self::ER_FILE_MOVING);
                        return false;
                    }
                } else {
                    if (!copy($uploadedFile, $this->getRootPath() . $file->path)) {
                        $file->delIncompleteRecord($fileID);
                        $this->setError(self::ER_FILE_COPYING);
                        return false;
                    }
                }
            }
        }

        // make resize (if needed)
        if ($file->isImage() && $resize = $this->getResizeList()) {
            $resizer = new FMResizer($this, $file);
            $resizer->initiateResize();
        }

        // complete file record
        if (!$file->completeFileRecord($this->getRootPath(false))) {
            return false;
        }

        return $file;
    }


    //todo:: wtf ?? mb delete this func
    public function addConstructFile($uploadedFile, $used = true, $delSource = true){


        // if uploaded using Yii form
        if ($uploadedFile instanceof UploadedFile) {
            $usedRuntime = 'yii';
        } elseif (!empty($uploadedFile)) {
            $usedRuntime = 'plupload';
        } else {
            $this->setError(self::ER_FILE_WRONG_INPUT);
            return false;
        }


        // generate file path


//        $path = DIRECTORY_SEPARATOR . $repository . DIRECTORY_SEPARATOR. implode(DIRECTORY_SEPARATOR, $split);

        $path = $_SERVER['DOCUMENT_ROOT'].'../../../public_html/content/files/catalog1';


        if (!is_dir($path)) {
            if (!mkdir($path, 0777, true)) {
                $this->setError(self::ER_PATH_CREATION_FAILED);
                return false;
            }
        }




        if (!copy($uploadedFile['path'], $path .   '/' . $uploadedFile['original_name'])) {
            unlink($uploadedFile['path']);
            $this->setError(self::ER_FILE_MOVING_UPLOADED);
            return false;
        }


        return   $path .   '/' . $uploadedFile['original_name'];



    }

    /**
     * @param integer $oldId - record id of old file to be deleted
     * @param UploadedFile $uploadedFile
     * @param bool    $used
     * @param bool    $delSource
     * @return bool|FMFile
     * deletes old file by id and ads new (just addFile/deleteFiles combo)
     */
    public function replaceFile($oldId, $uploadedFile, $used = true, $delSource = true){
        /** @var FMFile $file */
        $file = $this->addFile($uploadedFile, $used, $delSource);

        if (!$file->id) {
            return false;
        }

        $this->deleteFiles($oldId);
        return $file;
    }

    /**
     * @param $fileIdList - single id or list of ids in array or string (comma divided)
     * @param $checkRelation - if true all files will be checked
     *  whether they belong to active repository and deleted only in this case
     * @return bool|int false or number of deleted files
     * delete file, all file resizes and record in files table
     */
    public function deleteFiles($fileIdList, $checkRelation = false){
        $fileIdList = $this->checkFileIds($fileIdList);

        if (empty($fileIdList))
            return false;

        $resizeKeys = $this->getResizeKeyList();

        $deletedCounter = 0;
        foreach($fileIdList as $fileID) {
            $file = FMFile::findOne($fileID);


            if ($file) {
                if ($checkRelation) {
                    if (is_numeric($this->getId()) && $file['repository_id'] != $this->getId()) {
                        continue;
                    }

                    if (is_numeric($this->getSection()) && $file['section'] != $this->getSection()) {
                        continue;
                    }

                    // TODO add error throwing here
                }

                // delete main file
                if (file_exists($this->getRootPath() . $file->path))
                    unlink($this->getRootPath() . $file->path);

                // delete resizes (if exists)
                if ($resizeKeys !== false) {
                    foreach($resizeKeys as $key) {
                        if (file_exists($path = self::getResizedFileName($this->getRootPath() . $file->path, $key)))
                            unlink($path);
                    }
                }

                // delete table record
                if ($file->delete()) {
                    $deletedCounter++;
                }
            }
        }

        return $deletedCounter;
    }

    /**
     * @param FMFile $file
     * @return bool
     *
     * ! using FileValidator just doesn't fit for such file managing
     */
    public function checkFile($file){
        // check if its not image but the repository is set only for image files
        if ($this->getRepositoryType() === self::REPOSITORY_TYPE_IMAGE
            && !$file->isImage()) {

            $this->setError(self::ER_CHECK_MIME);
            return false;
        }

        // TODO check restricted mime

        // check uploaded file size
        if ($limits = $this->getSizeLimits()) {
            if ($limits[0] > 0 && $file->size < $limits[0]) {
                $this->setError(self::ER_CHECK_SIZE_MIN);
                return false;
            }
            if ($limits[1] > 0 && $file->size > $limits[1]) {
                $this->setError(self::ER_CHECK_SIZE_MAX);
                return false;
            }
        }

        // check uploaded file extension
        if ($allowed = $this->getAllowedExt()) {
            if (!in_array($file->ext, $allowed) && !empty($allowed)) {
                $this->setError(self::ER_CHECK_EXT);
                return false;
            }
        }

        return true;
    }

    /**
     * @param $idList
     * @return bool|int
     * set chosen files as "used" in files table
     */
    public function setUsedFiles($idList){
        $idList = $this->checkFileIds($idList);

        return (empty($idList))
            ? false
            : Yii::$app->db->createCommand()
                ->update(self::FILES_TABLE, array('used' => 1), array('in', 'id', $idList));
    }

    /**
     * @param FMFile $file
     * @param bool $repository
     * @return bool|string
     * generates path for file according to its id in files table
     * and repository settings (name and nesting level)
     */
    private function generateFilePath($file, $repository = false){


        if(empty($file->id)) {
            $file->id = $file->getPrimaryKey();
        }


        $hash = md5($file->id);
        $split = $this->splitStr($hash, $this->getNesting());



        if ($split === false) {
            $this->setError(self::ER_PATH_GENERATION_FAILED);
            return false;
        }

        if ($repository === false) {
            $repository = $this->getRepositoryName();
            if (!$repository)
                return false;
        }

//        $path = DIRECTORY_SEPARATOR . $repository . DIRECTORY_SEPARATOR
//            . implode(DIRECTORY_SEPARATOR, $split);
        $path = '/' . $repository . '/' . implode('/', $split);

        if (!is_dir($this->getRootPath() . $path)) {
            if (!mkdir($this->getRootPath() . $path, 0777, true)) {
                $this->setError(self::ER_PATH_CREATION_FAILED);
                return false;
            }
        }

        return $path . /*DIRECTORY_SEPARATOR*/ '/'
            . $file->id . '.' . $file->ext;
    }

    /**
     * @param int   $fileID
     * @param array $fields - fields to be selected from files table
     * @return mixed
     * info about file by its id
     */
    public function getFileInfo($fileID, $fields = array('path')) {
        return (new Query())
            ->select($fields)
            ->from(self::FILES_TABLE)
            ->where('id=:file_id', array(':file_id'=>$fileID))
            ->one();
    }

    public function getFileCount($sectionId) {
        if (!$this->_activeRepo) {
            $this->setError(self::ER_REPOSITORY_NOT_SPECIFIED);
            return false;
        }

        if (is_numeric($sectionId)) {
            $this->setSection($sectionId);
        } elseif($sectionId === false && !$this->_activeSection) {
            $this->setError(self::ER_SECTION_ID_NOT_SPECIFIED);
            return false;
        }

        return (new Query())
            ->select('COUNT(*) as count')
            ->from(self::FILES_TABLE)
            ->where(
                'repository_id=:repo_id AND section=:section',
                [':repo_id'=>$this->getId(),':section'=>$this->getSection()]
            )
            ->scalar();
    }

    /**
     * @param bool  $sectionId - id of the section to get files from
     * @param bool  $limit 
     * @param bool  $offset - limit+offset - kind of paging files
     * @param array $fields - fields to be returned from files table
     * @param bool  $usedOnly (not used temporary -> TODO)
     * @return array|bool
     * get file list from files table using section id
     */
    public function getFileList($sectionId = false, $limit = false, $offset = false, $fields = array('path'), $usedOnly = false) {
        if (!$this->_activeRepo) {
            $this->setError(self::ER_REPOSITORY_NOT_SPECIFIED);
            return false;
        }

        if (is_numeric($sectionId)) {
            $this->setSection($sectionId);
        } elseif($sectionId === false && !$this->_activeSection) {
            $this->setError(self::ER_SECTION_ID_NOT_SPECIFIED);
            return false;
        }

        // indexes can be forced here manually if needed (rep_id or rep_id + section)
        $command = (new Query())
            ->select($fields)
            ->from(self::FILES_TABLE)
            ->where('repository_id=:repo_id AND section=:section',
                array(':repo_id'=>$this->getId(),':section'=>$this->getSection()))
            ->orderBy('position ASC');

        if (is_numeric($limit)) {
            $command->limit($limit);
        }
        $command->offset((is_numeric($offset)) ? $offset : 0);


        return $command->all();
    }

    /* ----------- FILE NAMING AND LINK GENERATION ------------- */

    /**
     * @return array|bool - list of names of resizes for chosen repository
     * for right name formation
     */
    private function getResizeKeyList() {
        $resizeKeys = false;
        // check whether there must be resized files to delete
        if ($resize = $this->getResizeList()) {
            $resizeKeys = array();
            // get resize keys (do it once here, not in foreach)
            foreach($resize as $name => $param) {
                $resizeKeys[] = $name;
            }
        }

        return $resizeKeys;
    }

    public function getLink($fileId, $resize = ''){
       $fileInfo = $this->getFileInfo($fileId);
       $resizedImagePath = self::getResizedFileName($fileInfo['path'], $resize);

       return is_file(Yii::getAlias('@app') . $resizedImagePath)
           ? $resizedImagePath : false;
   }

    public function getDownloadLink($fileId, $repository = false){
        // TODO need this?
    }

    public function getSecureDownloadLink($fileId, $repository = false){
        // TODO
    }

    /**
     * @param $fileName - raw file name or name of any resize (any modifier is possible)
     * @param $resize - new modifier to be included into file path
     * @return bool|string
     * path for chosen file resize (just changing modifier)
     */
    public static function getResizedFileName($fileName, $resize) {
        if (empty($fileName)){
            return false;
        }

        $pathInfo = pathinfo($fileName);
        return $pathInfo['dirname'] . DIRECTORY_SEPARATOR
            . preg_replace("/^(.*?)(_[^_]*)?$/", "$1_".$resize, $pathInfo['filename'])
            . '.'. $pathInfo['extension'];
    }

    /* --------- WORKING WITH RAW FILES (UPLOAD/DOWNLOAD) ---------- */

    /**
     * @param integer $id file id
     * tries finding chosen file and send it to user
     */
    public static function getFile($id) {
        if (!($file = FMFile::findOne($id))) {
            header ("HTTP/1.0 404 Not Found");
            exit;
        }

        $absPath = $_SERVER['DOCUMENT_ROOT'] . $file->path;

        if (!file_exists($absPath)) {
            header ("HTTP/1.0 404 Not Found");
            exit;
        }

        $name = $file->original_name . "." . $file->ext;
        $fileSize = filesize($absPath);
        $fileTime = date("D, d M Y H:i:s T", $file->date);
        // check if file can be read
        $fd = fopen($absPath, "rb");

        if (!$fd){
          header ("HTTP/1.0 403 Forbidden");
          exit;
        }

        // check whether file can be send as partial content (using HTTP_RANGE)
        $range = 0;
        if (isset($_SERVER["HTTP_RANGE"])) {
            $range = $_SERVER["HTTP_RANGE"];
            $range = str_replace("bytes=", "", $range);
            $range = str_replace("-", "", $range);
            if (!empty($range)) {fseek($fd, $range);}
        }
        $content = fread($fd, $fileSize);
        fclose($fd);

        if ($range > 0) {
            header("HTTP/1.1 206 Partial Content");
        } else {
            header("HTTP/1.1 200 OK");
        }

        // send appropriate headers and file body itself
        header("Content-Disposition: attachment; filename=".$name);
        header("Last-Modified: $fileTime");
        header("Accept-Ranges: bytes");
        header("Content-Length: ".($fileSize-$range));
        header("Content-Range: bytes $range - ".($fileSize -1) . "/" . $fileSize);
        header("Content-type: application/octet-stream");

        print $content;
        exit;
    }

    /**
     * @param string $fileInputName - file input name (for upload in $_FILES['{name}'])
     * @return array - path to file and it's original name
     * Plupload file uploading function
     * get file from tmp dir
     * create single file (if uploading was multipart)
     */
    public function uploadFile($fileInputName = 'file') {
        // HTTP headers for no cache etc
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $contentType = false;
        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

        // Settings
        $targetDir = $this->_tmpPath;
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"])
            ? $_REQUEST["name"]
            : (isset($_FILES[$fileInputName]['name']) ? $_FILES[$fileInputName]['name'] : '');

        // Clean the fileName for security reasons
        $originalName = $fileName;
        //$fileName = iconv('UTF-8', 'windows-1251', $fileName);
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Create target dir
        if (!is_dir($targetDir))
            @mkdir($targetDir, 0777, true);

        // Remove old temp files
        if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
            while (($file = readdir($dir)) !== false) {
                $tmpFilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpFilePath) < time() - $maxFileAge) && ($tmpFilePath != "{$filePath}.part")) {
                    @unlink($tmpFilePath);
                }
            }

            closedir($dir);
        } else
            $this->setError("Plupload :: Не удалось записать файл во временную директорию");


        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES[$fileInputName]['tmp_name']) && is_uploaded_file($_FILES[$fileInputName]['tmp_name'])) {

                //$originalName = $_FILES['file']['name'];

                // Open temp file
                $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = fopen($_FILES[$fileInputName]['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        $this->setError("Plupload :: Failed to open input stream.");

                    fclose($in);
                    fclose($out);
                    @unlink($_FILES[$fileInputName]['tmp_name']);
                } else
                    $this->setError("Plupload :: Failed to open output stream.");
            } else
                $this->setError("Plupload :: Не удалось переместить закачанный файл");
        } else {
            // Open temp file
            $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = fopen("php://input", "rb");

                //$originalName = $fileName;

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    $this->setError("Plupload :: Failed to open input stream.");

                fclose($in);
                fclose($out);
            } else
                $this->setError("Plupload :: Failed to open output stream.");
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            //$name = preg_replace('#(.*?)\/[^\/]*$#', '$1/'.$originalName, $fileName);
            @rename("{$filePath}.part", $filePath);
        }

        return array('path' => $filePath, 'original_name' => $originalName);
    }

    /*----------- HELPER FUNCTIONS ------------*/

    /**
     * @param     $hash - file id hash
     * @param     $nesting - num of parts to split
     * @param int $block - split block length
     * @return array|bool
     * function generates parts of the path to the file according to nesting level
     * @example:
     *      fileId = 1 -> HASH: c4ca3248a0b923820dcc509a6f75849b
     *      nesting level = 4
     *      result is array containing parts of the path for chosen file -> c4/ca/32/48
     */
    private function splitStr($hash, $nesting, $block = self::HASH_SPLIT_LEN) {
        $needSize = $nesting * $block;
        if (strlen($hash) >= $needSize) {
            $split = array();
            for ($level = 0; $level < $nesting; $level++) {
                $split[] = substr($hash, $block * $level, $block);
            }

            return $split;
        }

        return false;
    }

    /**
     * @param $idList array|string list of file ids
     * @return array list of checked ids
     * checks if there are only valid ids in the list (positive numbers)
     */
    public function checkFileIds($idList){
        $checked = array();

        if (!is_array($idList)) {
            $idList = explode(',', $idList);
        }

        foreach($idList as $id) {
            if (is_numeric($id) && $id > 0) {
                $checked[] = $id;
            }
        }

        return $checked;
    }

    /**
     * @param $size
     * @return bool|int
     * converts text sizes into numeric form
     * @example 2K -> 2048 (2 * 1024)
     */
    private function getNumericSize($size) {
            // get size from symbol value
            $multipliers = array('K' => 1024, 'M' => 1048576, 'G' => 1073741824);

            if (is_numeric($size) && $size >= 0) {
                return $size;
            } elseif (preg_match("#^([\d]+)([\w]{1})$#", $size, $res)) {
                $mult = strtoupper($res[2]);
                if (array_key_exists($mult, $multipliers)) {
                    return $res[1] * $multipliers[$mult];
                } else {
                    $this->setError('РќР°СЃС‚СЂРѕР№РєРё СЂРµРїРѕР·РёС‚РѕСЂРёСЏ РЅРµРІРµСЂРЅС‹');
                    return false;
                }
            } else {
                $this->setError('РќР°СЃС‚СЂРѕР№РєРё СЂРµРїРѕР·РёС‚РѕСЂРёСЏ РЅРµРІРµСЂРЅС‹');
                return false;
            }
        }

    /*------------- ERROR HANDLING -------------*/

    /**
     * @param $erCode integer add error to error list (by its code)
     */
    public function setError($erCode) {
        // TODO fill all error texts
        switch($erCode) {
            case self::ER_REPOSITORY_ALREADY_EXISTS:
                $this->_errors[] = 'Репозиторий с таким именем уже существует';
                break;
            case self::ER_REPOSITORY_NOT_EXIST:
                $this->_errors[] = 'Репозитория с указанным именем не существует';
                break;
            case self::ER_CHECK_EXT:
                $this->_errors[] = 'Доступные расширения для загрузки: ' . implode(', ', $this->getAllowedExt());
                break;
            case self::ER_REPOSITORY_NOT_SPECIFIED:
                $this->_errors[] = 'Не указан репозиторий';
                break;
            default:
                $this->_errors[] = $erCode;
                break;
        }
    }

    public function errorsOccurred() {
        return (empty($this->_errors)) ? false : true;
    }

    public function getErrors() {
        return $this->_errors;
    }
}
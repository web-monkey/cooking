<?php

namespace common\components;


use yii\base\Component;
use yii\web\UploadedFile;

/**
 * component for resizing images
 *
 * v 0.0.1 пока Фигурирует  FileManager убрать либо адаптировать вычистив мусор.
 *
 */
class Resize extends Component
{
    public static $_instance;
    public $fillColor = [255,255,255,1];

    private $file;
    /**
     * @var FileManager
     */
    public $fm;
    /**
     * @var integer image width
     */
    private $_width;
    /**
     * @var integer image height
     */
    private $_height;
    /**
     * @var float image proportion
     * set as $width / $height
     */
    private $_proportion;
    /**
     * @var string image type (used in resize function for working on transparency)
     */
    private $_type;

    private $_settings;

    const TYPE_CUT = 'cut';
    const TYPE_FILL = 'fill';
    const TYPE_SQUEEZE = 'squeeze';
    


    public static function getInstance() {
        return (isset(self::$_instance))
            ? self::$_instance
            : self::$_instance = new Resize();
    }

    public function getResizeList() {
        return (empty($this->_settings['resize']))
            ? false
            : ((is_array($this->_settings['resize']))
                ? $this->_settings['resize']
                : unserialize($this->_settings['resize']));
    }

    /**
     * @param FileManager $fm - "parent" FileManager instance
     */
    /*function __construct(&$fm, $file) {
        $this->fm 	= $fm;
        $this->file = $file;
    }
    */
    /**
     * @return bool
     * makes copies of selected file and generate resizes
     * using the resize list from repository settings
     */
    public function initiateResize() {
        $filePath = $this->fm->getRootPath() . str_replace('/'.FileManager::FILES_ROOT_DIRECTORY, '', $this->file->path);

        if (!file_exists($filePath)) {
            $this->fm->setError(FileManager::ER_RESIZER_FNF);
            return false;
        }

        // get list of needed resizes
        $resizeList = $this->fm->getResizeList();
        // set type
        $this->setType($this->file->mime);

        // if file needs resize
        if ($resizeList){
            // getting original size and setting the proportion
            $info = getimagesize($filePath);
            list($this->_width, $this->_height) = $info;
            $this->_proportion = $this->_width/$this->_height;

            foreach ($resizeList as $item => $set) {
                // make copy of the image and resize it
                $newFileName = self::getResizedFileName($filePath, $item);

                if ($this->makeImageCopy($filePath, $newFileName)) {
                    $defaultSet = array(
                        'type'	=> self::TYPE_CUT,
                        'w'		=> 0,
                        'h'		=> 0
                    );
                    $set = array_merge($defaultSet, $set);

                    // check if resize is needed
                    if (
                        ($set['w'] > 0 && $set['h'] == 0 && $this->_width <= $set['w'])
                        || ($set['h'] > 0 && $set['w'] == 0 && $this->_height <= $set['h'])
                        || ($set['w'] > 0 && $set['h'] > 0 && $this->_width <= $set['w'] && $this->_height <= $set['h'])
                    ) {
                        continue;
                    }

                    $color = (isset($set['color'])) ? $set['color'] : $this->fillColor;
                    $this->resize($newFileName, $set['w'], $set['h'], $set['type'], $color);
                }
            }
        }

        return true;
    }

    public function reduceTo($set) {
        $w = $h = 0;
        if (isset($set['w'])) { $w = $set['w']; }
        if (isset($set['h'])) { $h = $set['h']; }
        $type = empty($set['type']) ? self::TYPE_CUT : $set['type'];

        if ($w != 0 || $h != 0) {
            $filePath = \Yii::getAlias('@app'). $this->file->path;
            $info = getimagesize($filePath);
            list($this->_width, $this->_height) = $info;
            $this->_proportion = $this->_width/$this->_height;

            $this->setType($this->file->mime);

            return $this->resize($filePath, $w, $h, $type);
        }

        return false;
    }

    /**
     * @param string $file - path to file to be resized
     * @param int    $w - new width
     * @param int    $h - new height
     * @param string $type - resize type
     * @param array  $color - fill color for background (for type self::TYPE_FILL)
     * @return bool
     *
     * function counts new image proportion
     * makes copy of chosen file and resize it
     *
     * if width is not defined resize proportion will be counted only by height
     * (the same goes for width when height is not defined)
     *
     * resize types:
     * self::TYPE_CUT - proportion is counted the way that image will be cropped to fit the new size
     * self::TYPE_FILL - image is middled with out proportion change and empty fields are filled with $color
     */
    private function resize($file, $w = 0, $h = 0, $type = self::TYPE_CUT, $color) {

        if(empty($color)) {
            $color = $this->fillColor;
        }

        if (!file_exists($file)) {
            $this->fm->setError(FileManager::ER_RESIZER_FNF);
            return false;
        }

        if ($w == 0 && $h == 0) {
            $this->fm->setError(FileManager::ER_RESIZER_INVALID_PARAMS);
            return false;
        }

        if ($w == 0) {
            $w = round($h * $this->_proportion);
        } elseif ($h == 0) {
            $h = round($w / $this->_proportion);
        }
        $newProportion = $w/$h;

        switch ($type) {
            case self::TYPE_CUT:
                if ($this->_proportion > $newProportion) {
                    $srcW = round($this->_height * $newProportion);
                    $srcH = $this->_height;
                    $srcX = round(($this->_width - $srcW)/2);
                    $srcY = 0;
                } else {
                    $srcW = $this->_width;
                    $srcH = round($this->_width / $newProportion);
                    $srcX = 0;
                    $srcY = round(($this->_height - $srcH)/2);
                }

                if ($sourceImage = $this->openImage($file)) {
                    $newImage = imagecreatetruecolor($w, $h);

                    // saving transparency for png images
                    if ($this->_type == 'png') {
                        imagealphablending ($newImage, false);
                        imagesavealpha($newImage, true);
                    }

                    imagecopyresampled($newImage, $sourceImage, 0, 0, $srcX, $srcY, $w, $h, $srcW, $srcH);

                    return $this->saveImage($newImage, $file);
                }

                break;
            case self::TYPE_FILL:
                if ($this->_proportion > $newProportion) {
                    $filledWidth = $w;
                    $filledHeight = $this->_height / ($this->_width / $w);
                    $dstX = 0;
                    $dstY = round(($h - $filledHeight)/2);
                } else {
                    $filledWidth = $this->_width * ($h / $this->_height);
                    $filledHeight = $h;
                    $dstX = round(($w - $filledWidth)/2);
                    $dstY = 0;
                }

                if ($sourceImage = $this->openImage($file)) {
                    $newImage = imagecreatetruecolor($filledWidth, $filledHeight);

                    // saving transparency for png images
                    if ($this->_type == 'png') {
                        imagealphablending ($newImage, false);
                        imagesavealpha($newImage, true);
                    }

                    imagecopyresampled($newImage, $sourceImage, 0, 0, 0, 0,
                        $filledWidth, $filledHeight, $this->_width, $this->_height);

                    $emptyImage = imagecreatetruecolor($w, $h);

                    if ($this->_type == 'png') {
                        imagealphablending ($emptyImage, false);
                        imagesavealpha($emptyImage, true);
                    }

                    $color = imagecolorallocatealpha($emptyImage, $color[0], $color[1], $color[2], $color[3]);
                    imagefill($emptyImage, 0, 0, $color);

                    imagecopy($emptyImage, $newImage, $dstX, $dstY, 0, 0, $filledWidth, $filledHeight);
                    return $this->saveImage($emptyImage, $file);
                }

                break;
            case self::TYPE_SQUEEZE:
                if ($this->_proportion > $newProportion) {
                    $filledWidth = $w;
                    $filledHeight = $w / $this->_proportion;
                } else {
                    $filledWidth = $h * $this->_proportion;
                    $filledHeight = $h;
                }

                if ($sourceImage = $this->openImage($file)) {
                    $newImage = imagecreatetruecolor($filledWidth, $filledHeight);

                    // saving transparency for png images
                    if ($this->_type == 'png') {
                        imagealphablending ($newImage, false);
                        imagesavealpha($newImage, true);
                    }

                    imagecopyresampled($newImage, $sourceImage, 0, 0, 0, 0,
                        $filledWidth, $filledHeight, $this->_width, $this->_height);

                    $emptyImage = imagecreatetruecolor($filledWidth, $filledHeight);

                    if ($this->_type == 'png') {
                        imagealphablending ($emptyImage, false);
                        imagesavealpha($emptyImage, true);
                    }

                    $color = imagecolorallocatealpha($emptyImage, $color[0], $color[1], $color[2], $color[3]);
                    imagefill($emptyImage, 0, 0, $color);

                    imagecopy($emptyImage, $newImage, 0, 0, 0, 0, $filledWidth, $filledHeight);
                    return $this->saveImage($emptyImage, $file);
                }

                break;
        }

        return false;
    }


    /**
     * @version aap v. 0.1
     */


    /**
     * @param $model
     * @param $attribute
     * @param $resize bool 
     * 
     * how it use ?
     * $uploader = Resize::getInstance() || Yii::$app->resize ( use Resize  getInstance or  as component) ;
     * $uploader->uploadImage($model, 'cover');
     * 
     */
    public function uploadImage(&$model, $attribute, $resize = true)
    {
        $image = UploadedFile::getInstance($model, $attribute);

        //upload new image and resize;
        if (!empty($image)) {

            //delete old images before upload new
            if(!empty($model->{$attribute})) {
                $this->deleteImage($model,$attribute);
            }

            $this->_settings = method_exists($model,'repositoryList') ? $model::repositoryList($attribute) : $attribute;

            $path = is_array($this->_settings) ? self::makePath($this->_settings['name']) : self::makePath($this->_settings);
            $fileName =  time().'.'.$image->extension;

            //save image

            if($image->saveAs($path['abs'].$fileName)) {

                $model->{$attribute} = $path['rel'].$fileName;
                //save resize
                if($resize && is_array($this->_settings)) {
                    $this->imgResize($path['abs'].$fileName);
                }
            }
        }
    }

    public function uploadImages(&$model, $attribute, $resize = true)
    {
        $imageList = UploadedFile::getInstances($model, $attribute);

        //delete old images before upload new


        //upload new image and resize;
        if (!empty($imageList)) {


            if(!empty($model->{$attribute}) && !empty($model->{$attribute}[0])) {
                $this->deleteImages($model, $attribute);
            }

            $this->_settings = method_exists($model,'repositoryList') ? $model::repositoryList($attribute) : $attribute;

            $imageIds = [];
            
            if(!empty($this->_settings) && array_key_exists('type',$this->_settings) && $this->_settings['type'] == 'gallery') {


                foreach ($imageList as $sort => $image) {

                    $path = self::makePath($this->_settings['name']);
                    $fileName =  time().'_'.intval(rand(1,1000)).'.'.$image->extension;

                    if($image->saveAs($path['abs'].$fileName)) {

                        if($resize && is_array($this->_settings)) {
                            $this->imgResize($path['abs'].$fileName);
                        }

                        $img = new FileList();
                        $img->path = $path['rel'].$fileName;
                        $img->_sort = $sort;

                        if($img->save()) {
                            $imageIds[] = $img->getPrimaryKey();
                        }

                    }
                }

            }


            if(!empty($imageIds)) {
                $model->{$attribute} = implode(',',$imageIds);

            }
        }
    }




    /**
     * @param $model
     * @param $attribute
     */
    public function deleteImage(&$model, $attribute)
    {
        $path = \Yii::getAlias('@frontend/web'.$model->{$attribute});

        if(!empty($path) && file_exists($path)) {

            unlink($path);

            if(empty($this->_settings) && method_exists($model,'repositoryList')) {
                $this->_settings = $model::repositoryList($attribute);
            }


            if(!empty($this->_settings && array_key_exists('resize', $this->_settings))){
                $resizeList = $this->_settings['resize'];
            }

            if(!empty($resizeList)){

                foreach ($resizeList as $resize => $prop) {

                    $fileName = self::getResizedFileName($path, $resize);

                    if(!empty($fileName) && file_exists($fileName))  {
                        unlink($fileName);
                    }

                }
            }

            //remove empty folder;
            $dir = dirname($path);
            $files = array_diff(scandir($dir), ['..', '.']);

            if(empty($files)) {
                rmdir($dir);
            }

            $model->{$attribute} = null;
        }

    }

    public function deleteImages(&$model, $attribute)
    {

        if(!empty($model->{$attribute})) {

        $images = FileList::getGallery($model->{$attribute});

        if(!empty($images)) {

            foreach ($images as $img) {
                $path = \Yii::getAlias('@frontend/web'.$img['path']);

                if(!empty($path) && file_exists($path)) {

                    unlink($path);

                    if(empty($this->_settings) && method_exists($model,'repositoryList')) {
                        $this->_settings = $model::repositoryList($attribute);
                    }


                    if(!empty($this->_settings && array_key_exists('resize', $this->_settings))){
                        $resizeList = $this->_settings['resize'];
                    }

                    if(!empty($resizeList)){

                        foreach ($resizeList as $resize => $prop) {

                            $fileName = self::getResizedFileName($path, $resize);

                            if(!empty($fileName) && file_exists($fileName))  {
                                unlink($fileName);
                            }

                        }
                    }

                    //remove empty folder;
                    $dir = dirname($path);
                    $files = array_diff(scandir($dir), ['..', '.']);

                    if(empty($files)) {
                        rmdir($dir);
                    }

                }
            }
        }

        $model->{$attribute} = '';
        }
    }


    /**
     * @param $filePath
     * @param array $resizeList
     * @return bool
     *
     * resize example
     *
     * 
     public static function repositoryList($key = null) {
    
        $repo = [
            'cover' => [
                'allowed_ext' => 'jpg, jpeg, png, gif',
                'type' => 'image',//gallery
                'max_size' => '15M',
                'resize' => [
                    'preview' => array(
                        'w' => 120,
                        'h' => 157,
                        'type' => 'fill'
                    ),
                    'full_wide' => array(
                        'w' => 680,
                        'h' => 480,
                        'type' => 'fill'
                    ),
                    'full_high' => array(
                        'w' => 375,
                        'h' => 508,
                        'type' => 'fill'
                    ),
                ],
            ],
        ];
    
        return empty($key) ? $repo : $repo[$key];
     }
     */
    public function imgResize($filePath, $resizeList = []){

        if(empty($resizeList)) {
            $resizeList = $this->_settings['resize'];
        }

        if(!file_exists($filePath) ) {
            return false;
        }

        $info = getimagesize($filePath);

        $image = ['image/gif', 'image/png', 'image/jpeg'];

        if(in_array($info['mime'], $image) && !empty($resizeList)) {

            list($this->_width, $this->_height) = $info;
            $this->_proportion = $this->_width/$this->_height;
            $this->setType($info['mime']);

            foreach ($resizeList as $resize => $resizeParam) {

                $resizeParam['w'] = array_key_exists('w', $resizeParam) ? $resizeParam['w']  : 0;
                $resizeParam['h'] = array_key_exists('h', $resizeParam) ? $resizeParam['h']  : 0;

                if (
                    ($resizeParam['w'] > 0 && $resizeParam['h'] == 0 && $this->_width <= $resizeParam['w'])
                    || ($resizeParam['h'] > 0 && $resizeParam['w'] == 0 && $this->_height <= $resizeParam['h'])
                    || ($resizeParam['w'] > 0 && $resizeParam['h'] > 0 && $this->_width <= $resizeParam['w'] && $this->_height <= $resizeParam['h'])
                ) {
                    continue;
                }

                $fileName = self::getResizedFileName($filePath, $resize); //241
                $color = (isset($resizeParam['color'])) ? $resizeParam['color'] : $this->fillColor;

                if(!file_exists($filePath)) {
                    continue;
                }

                if(copy($filePath, $fileName)) {
                    $this->resize($fileName, $resizeParam['w'], $resizeParam['h'], $resizeParam['type'], $color);
                }
            }
            
        }

        return true;
    }

    /**
     * @param int $len
     * @return string
     */
    private static function getSuffix($len = 3){

        $str = range('a','z');
        shuffle($str);
        $pathExtension = '';

        for($i = 0; $i < $len ; $i++) {
            $pathExtension .= $str[$i];
        }

        return $pathExtension;
    }
    /**
     * @param $mime
     * @return bool
     * reduce mime to image type (for local type checks)
     */
    private function setType($mime) {
        switch($mime) {
            case 'image/jpeg':
                $this->_type = "jpg";
                return true;
            case 'image/png':
                $this->_type = "png";
                return true;
            case 'image/gif':
                $this->_type = "gif";
                return true;
            default:
                return false;
        }
    }
    /**
     * @param $source - source file path
     * @param $destination - new file path
     * @return bool - true if success, false otherwise
     * makes copy of file
     */
    private function makeImageCopy($source, $destination) {
        if (!copy($source, $destination)) {
            $this->fm->setError(FileManager::ER_RESIZER_FILE_COPYING);
            return false;
        }

        return true;
    }
    /**
     * @param $file
     * @return bool|resource
     * creates image resource according to its type
     */
    private function openImage($file) {
        switch($this->_type)	{
            case 'jpg':
                return imagecreatefromjpeg($file);
            case 'png':
                return imagecreatefrompng($file);
            case 'gif':
                return imagecreatefromgif($file);
            default:
                $this->fm->setError(FileManager::ER_RESIZER_NOT_IMAGE);
                return false;
        }
    }
    /**
     * @param resource $image - resource of image to be saved
     * @param string $path - path for saving file
     * @param bool $rewrite - true if rewrite is allowed, false otherwise
     * @param int  $quality - saving quality (used for jpeg files only)
     * @return bool - true on success, false otherwise
     * saves image resource to file on given path
     */
    private function saveImage($image, $path, $rewrite = true, $quality = 100) {
        if (empty($path) || $image === false) {
            $this->fm->setError(FileManager::ER_RESIZER_FILE_SAVING);
            return false;
        }

        if(!$rewrite && file_exists($path)) {
            $this->fm->setError(FileManager::ER_RESIZER_FILE_REWRITE);
            return false;
        }

        switch($this->_type) {
            case 'jpg':
                if(!is_numeric($quality) || $quality < 0 || $quality > 100)
                    $quality = 100;
                return imagejpeg($image, $path, $quality);
            case 'png':
                return imagepng($image, $path);
            case 'gif':
                return imagegif($image, $path);
            default:
                return false;
        }
    }
    /**
     * @param $fileName - raw file name or name of any resize (any modifier is possible)
     * @param $resize - new modifier to be included into file path
     * @return bool|string
     * path for chosen file resize (just changing modifier)
     */
    public static function getResizedFileName($fileName, $resize = 'preview') {
        if (empty($fileName)){
            return false;
        }

        $pathInfo = pathinfo($fileName);
        return $pathInfo['dirname'] . '/'
        . preg_replace("/^(.*?)(_[^_]*)?$/", "$1_".$resize, $pathInfo['filename'])
        . '.'. $pathInfo['extension'];
    }
    /**
     *
     * @param $folder //files folder name
     * @return array
     */
    public static function makePath($folder){

        $filesPath = \Yii::getAlias('@frontend').DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.$folder;
        $pathExtension = self::getSuffix();

        if (!is_dir($filesPath)) {
            mkdir($filesPath, 0755, true);
        }

        if (!is_dir($filesPath.DIRECTORY_SEPARATOR.$pathExtension)) {
            mkdir($filesPath.DIRECTORY_SEPARATOR.$pathExtension, 0755, true);
        }

        return [
            'abs' => $filesPath.DIRECTORY_SEPARATOR.$pathExtension.DIRECTORY_SEPARATOR,
            'rel' => '/files/'.$folder.'/'.$pathExtension.'/'
        ];
    }

}
<?php
namespace common\helpers;

use yii\helpers\BaseHtml;
 
class Html extends BaseHtml
{
    /**
     * @param float $number
     * @return string
     */
    public static function currency($number = 0.0)
    {
        return number_format(self::currencyToNumber($number), 2, ',', ' ');
    }

    /**
     * @param $number
     * @return float
     */
    public static function currencyToNumber($number)
    {
        return empty(trim($number)) ? 0 : floatval(preg_replace('/[^\d,\,,\.]/', '', str_replace(',','.',(string)$number))) ;
    }

    public static function icon($class, $text = null, $type = 'fa')
    {
        return self::tag('i', $text,
            [
                'class' => $type.' '.$type.'-'.$class
            ]
        );
    }
}

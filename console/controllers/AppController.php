<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;


class AppController extends Controller {

    public $interactive = false;

    public function actionInit() {

        Yii::$app->runAction('migrate/up', ['interactive' => $this->interactive, 'migrationPath' => '@console/migrations']);
        Yii::$app->runAction('migrate/up', ['interactive' => $this->interactive, 'migrationPath' => '@yii/log/migrations/']);
        Yii::$app->runAction('migrate/up', ['interactive' => $this->interactive, 'migrationPath' => '@yii/rbac/migrations']);
        Yii::$app->runAction('rbac/init', ['interactive' => $this->interactive]);
    }

    public function actionDown() {
        Yii::$app->runAction('migrate/down', ['interactive' => false, 'migrationPath' => '@yii/rbac/migrations']);
        Yii::$app->runAction('migrate/down', ['interactive' => false, 'migrationPath' => '@yii/log/migrations/']);
        Yii::$app->runAction('migrate/down', ['interactive' => false, 'all', 'migrationPath' => '@console/migrations']);
    }

    public function actionRestart() {
        $this->actionDown();
        $this->actionInit();
    }

}
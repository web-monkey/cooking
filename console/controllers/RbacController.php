<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**  
 * https://anart.ru/yii2/2016/04/11/yii2-rbac-ponyatno-o-slozhnom.html
 * 
 * php yii migrate --migrationPath=@yii/rbac/migrations
 * 
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 *
    auth_item - эта таблица хранит роли и разрешения.
    auth_item_child - здесь задаются наследования.
    auth_assignment - тут пользователям назначаются позиции из auth_item. Т.е. пользователю (по user ID)
    можем назначить роль/разрешение (по названиям).
    auth_rule - здесь хранятся имена классов правил, которые хранятся в php файлах.
 *
 * Назначаем роль в методе afterSave модели User для динамически добавляемых
    $auth = Yii::$app->authManager;
    $editor = $auth->getRole('editor'); // Получаем роль editor
    $auth->assign($editor, $this->id); // Назначаем пользователю, которому принадлежит модель User

    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['viewAdmin']
                    ],
                ],
            ],
        ];
    }

    if (!\Yii::$app->user->can('updateNews')) {
        throw new ForbiddenHttpException('Access denied');
    }

    Здесь параметру roles передается массив ролей или разрешений, что в свою очередь в недрах системы вызывает проверку Yii::$app->user->can(‘viewAdmin’)).
    Кстати, с помощью Yii::$app->user->can() мы можем проверять не только наличие разрешения у роли, но и наличие роли.
    Yii::$app->user->can('editor')) вернет true, если текущему пользователю назначена роль editor.
    Получить массив ролей мы можем так: Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()).
    Имейте в виду, что если роль админа наследует роль редактора новостей, то для админа Yii::$app->user->can('editor')) вернет true.
*/

class RbacController extends Controller {

    public function actionInit() {

        $auth = Yii::$app->authManager;
        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        //==============роли============================//

        $root       = $auth->createRole('dev');
        $admin      = $auth->createRole('admin');
        $manager    = $auth->createRole('manager');
        $user       = $auth->createRole('user');

        $auth->add($root);
        $auth->add($admin);
        $auth->add($manager);
        $auth->add($user);

        # иерархия  ролей
        $auth->addChild($manager,       $user);
        $auth->addChild($admin,         $manager);
        $auth->addChild($root,          $admin);
        //==============роли============================/>


        //==============разрешения============================//
        //todo:: перегруппировать по: модуль->контроллеры->методы

        $permission = $auth->createPermission('viewAP');
        $permission->description = 'Вход в админку';
        $auth->add($permission);
        $auth->addChild($manager, $permission);


        $permission = $auth->createPermission('changeSettings');
        $permission->description = 'Настройки';
        $auth->add($permission);
        $auth->addChild($root, $permission);

        $permission = $auth->createPermission('users');
        $permission->description = 'Пользователи апминки';
        $auth->add($permission);
        $auth->addChild($admin, $permission);

        //==============разрешения============================/>


        $auth->assign($root, 1);

        echo "RBAC init \n";
    }

}
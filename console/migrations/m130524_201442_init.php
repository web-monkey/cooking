<?php

use \backend\modules\user\models\User;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public $tableName = '{{%admin_panel_user}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [

            'id'                        => $this->primaryKey(),
           /* 'firstname'                 => $this->string()->notNull(),
            'lastname'                  => $this->string()->notNull(),*/
            'auth_key'                  => $this->string()->notNull(),
            'password_hash'             => $this->string()->notNull(),
            'password_reset_token'      => $this->string()->unique(),
            'email'                     => $this->string()->notNull()->unique(),

            'role'                      => $this->smallInteger()->notNull()->defaultValue(User::ROLE_USER),
            'status'                    => $this->smallInteger()->notNull()->defaultValue(User::STATUS_INACTIVE),
            'created_at'                => $this->integer()->notNull(),
            'updated_at'                => $this->integer()->notNull(),
            'blocked_at'                => $this->integer()

        ], $tableOptions);


        //add default user;
        $password = 'HldfB23$';

        $this->insert($this->tableName,[

            'id'                        => 1,
            'email'                     => 'info@webmonkey.su',
            /*'firstname'                 => 'admin',
            'lastname'                  => '',*/

            'password_hash'             => Yii::$app->security->generatePasswordHash($password),
            'password_reset_token'      => Yii::$app->security->generateRandomString() . '_' . time(),
            'auth_key'                  => Yii::$app->security->generateRandomString(),

            'role'                      => User::ROLE_ADMIN,
            'status'                    => User::STATUS_ACTIVE,
            'created_at'                => time(),
            'updated_at'                => time()

        ]);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

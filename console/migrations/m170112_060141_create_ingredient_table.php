<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ingredient`.
 */
class m170112_060141_create_ingredient_table extends Migration
{
    /**
     * @inheritdoc
     *
     */
    public $tableName = '{{%ingredient}}';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'is_visible' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

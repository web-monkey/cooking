<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dish`.
 */
class m170112_061516_create_dish_table extends Migration
{
    public $tableName = '{{%dish}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'is_visible' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `recipe`.
 */
class m170113_062946_create_recipe_table extends Migration
{
    public $tableRecipe = '{{%recipe}}';
    public $tableIngredient = '{{%ingredient}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableRecipe, [
            'id' => $this->primaryKey(),
            'dish_id' => $this->integer(),
            'ingredient_id' => $this->integer(),
            'count' => $this->decimal(10,3)
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableRecipe);
    }
}

<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => 'frontend\modules\user\Module',
            //'shouldBeActivated' => true
        ],
        'api' => [
            'class' => 'frontend\modules\api\Module',
            'modules' => [
                'v1' => 'frontend\modules\api\v1\Module'
            ]
        ]
    ],
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'loginUrl' => ['/user/sign-in/login'],
            'enableAutoLogin' => true,
            'as afterLogin' => 'common\behaviors\LoginTimestampBehavior'
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '5598060',
                    'clientSecret' => 'emHXrqsjnJQ3CDy4gsv9',
                    'scope' => 'email'
                ],
                'instagram' => [
                    'class' => 'kotchuprik\authclient\Instagram',
                    'clientId' => 'b94398e124c64722ac248db0f3204966',
                    'clientSecret' => '04f44665d8eb4c5c85837180deb6f609',
                    //'scope' => 'basic,public_content',
                ],

                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '499231280286267',
                    'clientSecret' => '35de06015deacb002e7d8c46c71a2ee6',
                    'scope' => 'email,public_profile',
                    'attributeNames' => [
                        'name',
                        'email',
                        'first_name',
                        'last_name',
                    ]
                ],
                /*
                 * for use append to composer.json  "kotchuprik/yii2-odnoklassniki-authclient": "^1.1"
                 * 'odnoklassniki' => [
                    'class' => 'kotchuprik\authclient\Odnoklassniki',
                    'applicationKey' => getenv('ODNOKLASSNIKI_APPLICATION_KEY'),
                    'clientId' => getenv('ODNOKLASSNIKI_CLIENT_ID'),
                    'clientSecret' => getenv('ODNOKLASSNIKI_CLIENT_SECRET'),
                ],*/
            ]
        ],
        'request'=>[
            'class' => 'common\components\Request',
            'web'=> '/frontend/web'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

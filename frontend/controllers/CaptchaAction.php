<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\captcha\Captcha;
use yii\helpers\Url;
use yii\web\Response;

/**
 * CaptchaAction renders a CAPTCHA image.
 *
 * CaptchaAction is used together with [[Captcha]] and [[\yii\captcha\CaptchaValidator]]
 * to provide the [CAPTCHA](http://en.wikipedia.org/wiki/Captcha) feature.
 *
 * By configuring the properties of CaptchaAction, you may customize the appearance of
 * the generated CAPTCHA images, such as the font color, the background color, etc.
 *
 * Note that CaptchaAction requires either GD2 extension or ImageMagick PHP extension.
 *
 * Using CAPTCHA involves the following steps:
 *
 * 1. Override [[\yii\web\Controller::actions()]] and register an action of class CaptchaAction with ID 'captcha'
 * 2. In the form model, declare an attribute to store user-entered verification code, and declare the attribute
 *    to be validated by the 'captcha' validator.
 * 3. In the controller view, insert a [[Captcha]] widget in the form.
 *
 * @property string $verifyCode The verification code. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CaptchaAction extends \yii\captcha\CaptchaAction
{
    /**
     * @var  mixed main captcha symbols
     */
    public $symbolsMain;
    /**
     * @var  mixed captcha main abc
     */
    public $symbolsAdding;
    public $fontSize;
    /**
     * @var bool
     * маштаб
     */
    public $scale = true;
    /**
     * @var bool
     * наклон
     */
    public $incline = true;

    //public $imageLibrary = 'gd';

    /**
     * Generates a new verification code.
     * @return string the generated verification code
     */
    protected function generateVerifyCode()
    {
        if ($this->minLength > $this->maxLength) {
            $this->maxLength = $this->minLength;
        }
        if ($this->minLength < 3) {
            $this->minLength = 3;
        }
        if ($this->maxLength > 20) {
            $this->maxLength = 20;
        }
        $length = mt_rand($this->minLength, $this->maxLength);

        $symbols = empty($this->symbolsMain) ?  'bcdfghjklmnpqrstvwxyz' : $this->symbolsMain;
        $adding = empty($this->symbolsAdding) ?  'aeiou' : $this->symbolsAdding;

        $code = '';
        for ($i = 0; $i < $length; ++$i) {
            if ($i % 2 && mt_rand(0, 10) > 2 || !($i % 2) && mt_rand(0, 10) > 9) {
                $code .= $adding[mt_rand(0, 4)];
            } else {
                $code .= $symbols[mt_rand(0, 20)];
            }
        }

        return $code;
    }

    protected function renderImage($code)
    {
        if (isset($this->imageLibrary)) {
            $imageLibrary = $this->imageLibrary;
        } else {
            $imageLibrary = Captcha::checkRequirements();
        }



        if ($imageLibrary === 'gd') {
            return $this->renderImageByGD($code);
        } elseif ($imageLibrary === 'imagick') {
            return $this->renderImageByImagick($code);
        } else {
            throw new InvalidConfigException("Defined library '{$imageLibrary}' is not supported");
        }
    }


    /**
     * Renders the CAPTCHA image based on the code using GD library.
     * @param string $code the verification code
     * @return string image contents in PNG format.
     */
    protected function renderImageByGD($code)
    {
        $this->fontSize -= 4;

        $image = imagecreatetruecolor($this->width, $this->height);

        $backColor = imagecolorallocate(
            $image,
            (int) ($this->backColor % 0x1000000 / 0x10000),
            (int) ($this->backColor % 0x10000 / 0x100),
            $this->backColor % 0x100
        );
        imagefilledrectangle($image, 0, 0, $this->width, $this->height, $backColor);
        imagecolordeallocate($image, $backColor);

        if ($this->transparent) {
            imagecolortransparent($image, $backColor);
        }

        $foreColor = imagecolorallocate(
            $image,
            (int) ($this->foreColor % 0x1000000 / 0x10000),
            (int) ($this->foreColor % 0x10000 / 0x100),
            $this->foreColor % 0x100
        );

        $length = strlen($code);
        $box = imagettfbbox(10, 0, $this->fontFile, $code);
        $w = $box[4] - $box[0] + $this->offset * ($length - 1);
        $h = $box[1] - $box[5];
        $scale = ($this->scale) ?  min(($this->width - $this->padding * 2) / $w, ($this->height - $this->padding * 2) / $h) : $this->scale;
        $incline = ($this->incline) ? rand(-10, 10) : $this->incline;

        $x = 10;
        $y = round($this->height * 27 / 40);
        for ($i = 0; $i < $length; ++$i) {
            $fontSize = (int) (empty($this->fontSize)  ? (rand(26, 32) * $scale * 0.8) : $this->fontSize);

            $letter = $code[$i];
            $box = imagettftext($image, $fontSize, $incline, $x, $y, $foreColor, $this->fontFile, $letter);
            $x = $box[2] + $this->offset;
        }

        imagecolordeallocate($image, $foreColor);

        ob_start();
        imagepng($image);
        imagedestroy($image);

        return ob_get_clean();
    }

    /**
     * Renders the CAPTCHA image based on the code using ImageMagick library.
     * @param string $code the verification code
     * @return string image contents in PNG format.
     */
    protected function renderImageByImagick($code)
    {

        //todo:: пока не сделано но потом придумать как центрировать текст
        $backColor = $this->transparent ? new \ImagickPixel('transparent') : new \ImagickPixel('#' . dechex($this->backColor));
        $foreColor = new \ImagickPixel('#' . dechex($this->foreColor));

        $image = new \Imagick();
        $image->newImage($this->width, $this->height, $backColor);

        $draw = new \ImagickDraw();
        $draw->setFont($this->fontFile);
        $draw->setTextAlignment(\Imagick::ALIGN_CENTER);
        $draw->setFontSize($this->fontSize);
        $fontMetrics = $image->queryFontMetrics($draw, $code);


        $length = strlen($code);
        $w = (int) ($fontMetrics['textWidth']) - 8 + $this->offset * ($length - 1);// ширина текст?
        $h = (int) ($fontMetrics['textHeight']) - 8;

        $scale = ($this->scale) ?  min(($this->width - $this->padding * 2) / $w, ($this->height - $this->padding * 2) / $h) : $this->scale;
        $incline = ($this->incline) ? rand(-10, 10) : $this->incline;

        $x = 10;
        $y = round($this->height * 27 / 40);

        //$code = [$x];
        //пишем символы капчи.
        for ($i = 0; $i < $length; ++$i) {
            $draw = new \ImagickDraw();

            $draw->setFont($this->fontFile);
            $draw->setFontSize((int) (empty($this->fontSize)  ? rand(26, 32) * $scale * 0.8 : $this->fontSize) ); //
            $draw->setFillColor($foreColor);
            $image->annotateImage($draw, $x, $y, $incline, $code[$i]); // $draw, $x, $y, rand(-10, 10), $code[$i]
            $fontMetrics = $image->queryFontMetrics($draw, $code[$i]);
            $x += (int) ($fontMetrics['textWidth']) + $this->offset;
        }


        $image->setImageFormat('png');
        return $image->getImageBlob();
    }

}

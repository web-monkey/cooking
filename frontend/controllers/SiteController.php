<?php
namespace frontend\controllers;

use backend\modules\cooking\models\Dish;
use backend\modules\cooking\models\Ingredient;
use backend\modules\cooking\models\Recipe;
use Yii;
use common\models\LoginForm;
use frontend\modules\user\models\PasswordResetRequestForm;
use frontend\modules\user\models\ResetPasswordForm;
use frontend\modules\user\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use \Imagick;

/**
 * Site controller
 */
class SiteController extends \backend\controllers\SiteController
{
    const MIN_INGREDIENT_COUNT = 2;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'frontend\controllers\CaptchaAction',
                'backColor' => '16777215', // 'foreColor'=>0x33CC33,
                #'foreColor' => '16777215',
                'transparent' => true,
                'offset' => 2,
                'symbolsMain' => 'ABCDEFGHKLMNPQRSTUVXYZ',
                'symbolsAdding' => '2456789',
                'minLength' => 5,
                'maxLength' => 5,
                'width' => 80,
                'height' => 42,
                #'fontFile' => '@frontend/web/fonts/PTSansCaption/PTSansCaptionBold.ttf',
                'fontSize' => 18,
                'scale' => false,
                'incline' => false,
                'fixedVerifyCode' => '12345', //убрать фиксированый текст капчи
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $ingredients = Ingredient::find()
            ->active()
            ->all();

        return $this->render('index', ['ingredients' => $ingredients]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    public function actionGetDish()
    {
        $dishList = [];
        $ingredients = Yii::$app->request->getQueryParam('i', []);

        if (!empty($ingredients)) {

            $recipes = Recipe::find()
                ->joinWith(['dish'])
                ->where(
                    ['IN', 'ingredient_id', $ingredients]
                )
                ->andWhere(['dish.is_visible' => Dish::STATUS_VISIBLE])
                ->asArray()
                ->all();

            $recipeList = [];
            $complete = false;

            if (!empty($recipes)) {

                foreach ($recipes as $recipe) {

                    if (!array_key_exists($recipe['dish_id'], $recipeList)) {

                        $recipeList[$recipe['dish_id']] = $recipe['dish'];
                        $recipeList[$recipe['dish_id']]['specific'] = [];

                    }

                    $recipeList[$recipe['dish_id']]['specific'][] = $recipe['ingredient_id'];
                }

                if (!empty($recipeList)) {

                    foreach ($recipeList as $recipe) {


                        if (count($recipe['specific']) >= self::MIN_INGREDIENT_COUNT) {

                            if (!$complete && empty(array_diff($ingredients, $recipe['specific']))) {

                                $complete = true;
                                $dishList[] = $recipe;

                            } elseif (!$complete) {

                                $diff = array_diff($recipe['specific'], $ingredients);

                                if ($diff >= self::MIN_INGREDIENT_COUNT) {
                                    $dishList[] = $recipe;
                                }
                            }
                        }
                    }
                }

            }
        }

        return $this->echoJSON($dishList);
    }


}

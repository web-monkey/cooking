<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

$useSocialPages = false;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php  if($useSocialPages) { ?>

    <!-- если сайт многоязычный -->
    <!-- <meta property="og:locale" content="ru_RU"> -->
    <!-- open graph Facebook-->
    <meta property="og:title" content="Заголовок" />
    <meta property="og:type" content="website" />

    <meta property="og:url" content="http://localhost.my" />
    <!-- не меньше 600х315, не более 8Мб -->
    <meta property="og:image" content="http://localhost.my/img/og_cover.jpg" />
    <meta property="og:description" content="описание не длинее 155 символов" />
    <meta property="fb:admins" content="Facebook ID" />

    <!-- Twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@ник_компании_в_твиттере">
    <meta name="twitter:title" content="Заголовок">
    <meta name="twitter:description" content="описание не длинее 155 символов">
    <meta name="twitter:creator" content="@ник_в_твиттере">
    <!-- картинка не меньше 280х150, не более 1Мб -->
    <meta name="twitter:image" content="http://localhost.my/img/tw_cover.jpg">

    <!-- G+ / Pinterest -->
    <meta itemprop="name" content="Заголовок">
    <meta itemprop="description" content="описание не длинее 155 символов">
    <meta itemprop="image" content="http://localhost.my/img/g_cover.jpg">

    <!-- Google authorship -->
    <link rel="author" href="https://plus.google.com/[Google+_Profile]/posts"/>
    <link rel="publisher" href="https://plus.google.com/[Google+_Page_Profile]"/>
    <?php  } ?>


    <link rel="icon" href="/img/favicon/favicon-16.png" sizes="16x16" type="image/png">
    <link rel="icon" href="/img/favicon/favicon-32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/img/favicon/favicon-48.png" sizes="48x48" type="image/png">
    <link rel="icon" href="/img/favicon/favicon-62.png" sizes="62x62" type="image/png">
    <link rel="icon" href="/img/favicon/favicon-192.png" sizes="192x192" type="image/png">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap"><?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']]
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/user/sign-in/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/user/sign-in/login']];
    } else {
        $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
        'Logout (' . Yii::$app->user->identity->username . ')',
        ['class' => 'btn btn-link']
        )
        . Html::endForm()
        . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right">Powered by WMSK</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php
/** @var \backend\modules\cooking\models\Ingredient[] $ingredients */
?>
<div class="col-xs-2">
    <ul class="list-unstyled filter-ingredients">
        <?php foreach ($ingredients as $ingredient) { ?>
        <li>
            <div class="checkbox">
                <label>
                    <input type="checkbox" data-id="<?=$ingredient->id; ?>"> <?=$ingredient->name; ?>
                </label>
            </div>
        </li>
        <?php } ?>
    </ul>
</div>
<div class="col-xs-10">
    <table class="dish table table-bordered table-condensed ">
        <thead>
        <tr>
            <th>Блюда</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>
/**
 * Created by andrey on 16.01.2017.
 */
$(document).ready(function () {
    var $body = $('body');
    var $dishTable = $body.find('.dish.table tbody');
    var MIN_INGREDIENT_COUNT = 2;

    $body.on('change', '.filter-ingredients input[type=checkbox]', function () {
        var $wrap = $(this).closest('.filter-ingredients');
        var selected = [];
        $.each($wrap.find('input[type=checkbox]:checked'), function (i, el) {
            selected.push($(el).data('id'));
        });


        if (selected.length >= MIN_INGREDIENT_COUNT) {
            $.get('/site/get-dish', {i: selected}, function (data) {
                setRow((data.length) ? data : [{name: 'Ничего не найдено'}]);
            }, 'json');
        } else {
            setRow([{name: 'Выберите больше ингредиентов'}]);
        }

    });

    function setRow(data) {
        $dishTable.empty();


        if (data.length) {
            $.each(data, function (key, val) {
                $dishTable.append('<tr><td>' + val.name + '</td></tr>')
            });
        }
    }
});
